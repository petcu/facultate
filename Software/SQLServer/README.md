# Download SQL Server Express

**Source**: [http://downloadsqlserverexpress.com](http://downloadsqlserverexpress.com/) 

There's a funny blog post about how to download SQL Server Express [from Long Zheng](http://www.istartedsomething.com/20140616/the-12-step-process-to-download-microsoft-sql-server-express-2014/). It surprisingly how complex some companies make downloading things. I've always thought that a giant Download Now button is the best way, but perhaps that's just me?

Downloading SQL Server Express is unnecessarily hard, and it's made harder by the new Microsoft Download Center "download multiple files" interface that doesn't include descriptions or primary file recommendations. It should be a list of links, and you should be able to right click and Save As.

SQL Server 2019 Express Edition (English):

- Basic (239 MB): [https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SQLEXPR\_x64\_ENU.exe](https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SQLEXPR_x64_ENU.exe?WT.mc_id=-blog-scottha)
- Advanced (700 MB): [https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SQLEXPRADV\_x64\_ENU.exe](https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SQLEXPRADV_x64_ENU.exe?WT.mc_id=-blog-scottha)
- LocalDB (53 MB): [https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SqlLocalDB.msi](https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SqlLocalDB.msi?WT.mc_id=-blog-scottha)

SQL Server 2017 Express Edition (English):

- Core (275 MB): [https://download.microsoft.com/download/E/F/2/EF23C21D-7860-4F05-88CE-39AA114B014B/SQLEXPR\_x64\_ENU.exe](https://download.microsoft.com/download/E/F/2/EF23C21D-7860-4F05-88CE-39AA114B014B/SQLEXPR_x64_ENU.exe?WT.mc_id=-blog-scottha)
- Advanced (710 MB): [https://download.microsoft.com/download/E/F/2/EF23C21D-7860-4F05-88CE-39AA114B014B/SQLEXPRADV\_x64\_ENU.exe](https://download.microsoft.com/download/E/F/2/EF23C21D-7860-4F05-88CE-39AA114B014B/SQLEXPRADV_x64_ENU.exe?WT.mc_id=-blog-scottha)
- LocalDB (45 MB): [https://download.microsoft.com/download/E/F/2/EF23C21D-7860-4F05-88CE-39AA114B014B/SqlLocalDB.msi](https://download.microsoft.com/download/E/F/2/EF23C21D-7860-4F05-88CE-39AA114B014B/SqlLocalDB.msi?WT.mc_id=-blog-scottha)

SQL Server 2016 with SP2 Express Edition (English):

- Core (437 MB): [https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16\_sp2\_dlc/en-us/SQLEXPR\_x64\_ENU.exe](https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16_sp2_dlc/en-us/SQLEXPR_x64_ENU.exe?WT.mc_id=-blog-scottha)
- Advanced (1445 MB): [https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16\_sp2\_dlc/en-us/SQLEXPRADV\_x64\_ENU.exe](https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16_sp2_dlc/en-us/SQLEXPRADV_x64_ENU.exe?WT.mc_id=-blog-scottha)
- LocalDB (45 MB): [https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16\_sp2\_dlc/en-us/SqlLocalDB.msi](https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16_sp2_dlc/en-us/SqlLocalDB.msi?WT.mc_id=-blog-scottha)

SQL Server 2016 with SP1 Express Edition (English):

- Core (411 MB): [https://download.microsoft.com/download/9/0/7/907AD35F-9F9C-43A5-9789-52470555DB90/ENU/SQLEXPR\_x64\_ENU.exe](https://download.microsoft.com/download/9/0/7/907AD35F-9F9C-43A5-9789-52470555DB90/ENU/SQLEXPR_x64_ENU.exe?WT.mc_id=-blog-scottha)
- Advanced (1255 MB): [https://download.microsoft.com/download/9/0/7/907AD35F-9F9C-43A5-9789-52470555DB90/ENU/SQLEXPRADV\_x64\_ENU.exe](https://download.microsoft.com/download/9/0/7/907AD35F-9F9C-43A5-9789-52470555DB90/ENU/SQLEXPRADV_x64_ENU.exe?WT.mc_id=-blog-scottha)
- LocalDB (45 MB): [https://download.microsoft.com/download/9/0/7/907AD35F-9F9C-43A5-9789-52470555DB90/ENU/SqlLocalDB.msi](https://download.microsoft.com/download/9/0/7/907AD35F-9F9C-43A5-9789-52470555DB90/ENU/SqlLocalDB.msi?WT.mc_id=-blog-scottha)

I've done the HTTP sniffing and work, and written this blog post in the hopes that it helps you (and I'm bookmarking it for myself, for the future).

### [Download SQL Server 2017 Express](https://go.microsoft.com/fwlink/?linkid=853017&WT.mc_id=-blog-scottha)

### [Download SQL Server Management Studio 17.3](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?WT.mc_id=-blog-scottha)

### [Download SQL Server 2016 Express](https://go.microsoft.com/fwlink/?LinkID=799012&WT.mc_id=-blog-scottha)

**This release of SQL Server no longer bundles tools with SQL Server. Tools are a separate install.*

### [Download SQL Server 2016 Management Studio (August 2017) - 17.2](https://go.microsoft.com/fwlink/?linkid=854085&WT.mc_id=-blog-scottha) 

**[Download SQL Server Management Studio 17.2 Upgrade Package (upgrades 17.x to 17.2)](https://go.microsoft.com/fwlink/?linkid=854087&WT.mc_id=-blog-scottha)**

### [Download SQL Server 2016 Management Studio (June 2016 release)](http://go.microsoft.com/fwlink/?LinkID=799832&WT.mc_id=-blog-scottha)

**The installer works for both 32-bit and 64-bit machines and installs Management studio as well as command-line tools needed to manage SQL Server.*

### [Download SQL Server 2014 Express](http://www.microsoft.com/en-us/download/details.aspx?id=42299&WT.mc_id=-blog-scottha)

- You likely just want SQL Server 2014 Express with Tools. This download includes SQL Management Studio.
    - [SQL Server 2014 Express x64](http://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAndTools%2064BIT/SQLEXPRWT_x64_ENU.exe?WT.mc_id=-blog-scottha)
    - [SQL Server 2014 Express x86](http://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAndTools%2032BIT/SQLEXPRWT_x86_ENU.exe?WT.mc_id=-blog-scottha)
- Here's just SQL Server 2014 Management Studio
    - [SQL Management Studio x64](http://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/MgmtStudio%2064BIT/SQLManagementStudio_x64_ENU.exe?WT.mc_id=-blog-scottha)
    - [SQL Management Studio x86](http://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/MgmtStudio%2032BIT/SQLManagementStudio_x86_ENU.exe?WT.mc_id=-blog-scottha)
- SQL Server 2014 Express with Advanced Services
    - [Advanced Services x64](http://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAdv%2064BIT/SQLEXPRADV_x64_ENU.exe?WT.mc_id=-blog-scottha)
    - [Advanced Services x86](http://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAdv%2032BIT/SQLEXPRADV_x86_ENU.exe?WT.mc_id=-blog-scottha)

### [Download SQL Server 2012 Express](http://www.microsoft.com/en-us/download/details.aspx?id=29062&WT.mc_id=-blog-scottha)

- You likely just want SQL Server 2012 Express with Tools. This download includes SQL Management Studio.
    - [SQL Server 2012 Express x64](http://download.microsoft.com/download/8/D/D/8DD7BDBA-CEF7-4D8E-8C16-D9F69527F909/ENU/x64/SQLEXPRWT_x64_ENU.exe?WT.mc_id=-blog-scottha)
- Here's just SQL Server 2012 Management Studio
    - [SQL Management Studio x64](http://download.microsoft.com/download/8/D/D/8DD7BDBA-CEF7-4D8E-8C16-D9F69527F909/ENU/x64/SQLManagementStudio_x64_ENU.exe?WT.mc_id=-blog-scottha)
    - [SQL Management Studio x86](http://download.microsoft.com/download/8/D/D/8DD7BDBA-CEF7-4D8E-8C16-D9F69527F909/ENU/x86/SQLManagementStudio_x86_ENU.exe?WT.mc_id=-blog-scottha)
      

### [Download SQL Server 2008 Express R2 SP2](http://www.microsoft.com/en-us/download/details.aspx?id=30438&WT.mc_id=-blog-scottha)

- You likely just want SQL Server 2008 Express with Tools. This download includes SQL Management Studio.
    - [SQL Server 2008 Express x64](http://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLEXPRWT_x64_ENU.exe?WT.mc_id=-blog-scottha)
    - [SQL Server 2008 Express x86](http://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLEXPRWT_x86_ENU.exe?WT.mc_id=-blog-scottha)
- Here's just SQL Server 2008 Management Studio
    - [SQL Management Studio x64](http://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLManagementStudio_x64_ENU.exe?WT.mc_id=-blog-scottha)
    - [SQL Management Studio x86](http://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLManagementStudio_x86_ENU.exe?WT.mc_id=-blog-scottha)



