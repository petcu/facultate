# Maple12

- [Maple12](#maple12)
		- [Download](#download)
		- [Path - Place license.dat in](#path---place-licensedat-in)
		- [license.dat](#licensedat)

### Download

- Maple12WindowsInstaller - [Link1](https://ufile.io/rnv507lu) - [Link2](http://www.mediafire.com/file/pv63n56p9u1r4kp/file)
- Maple12LinuxX32Installer - [Link1](https://ufile.io/x73tankv) - [Link2](http://www.mediafire.com/file/stcjb20ywotkj1e/file)
- Maple12LinuxX86_64Installer - [Link1](https://ufile.io/nr3kwslw) - [Link2](http://www.mediafire.com/file/mame32gw12peg21/file)

### Path - Place license.dat in

Windows

 ```
C:\Program Files\Maple 12\license
 ```

Linux/Unix:

```
~/maple12/license/
```

### license.dat

```
FEATURE Maple12 maplelmg 2008.0416 permanent uncounted HOSTID=ANY \
	vendor_info=FEEKwLAGLLAOCvxBvGotwviAZ0PPvFCJtsytahqu \
	ISSUER=Maplesoft ISSUED=14-may-2008 NOTICE="TEAM TBE" \
	SN=2011-11-11 TS_OK SIGN=3D79EFD471B8
FEATURE Maple12Excel maplelmg 2008.0416 permanent uncounted \
	HOSTID=ANY \
	vendor_info=FEEKwLAGLLAOCvxBvGotuKumnuBqLSIstJRzeDpq \
	ISSUER=Maplesoft ISSUED=14-may-2008 NOTICE="TEAM TBE" \
	SN=2011-11-11 TS_OK SIGN=ED5DD924E950
FEATURE MapleReader12 maplelmg 2008.0416 permanent uncounted \
	HOSTID=ANY \
	vendor_info=FEEKwLAGLLAOCvxBwGotxtutlsTgqGxDgzzFints \
	ISSUER=Maplesoft ISSUED=14-may-2008 NOTICE="TEAM TBE" \
	SN=2011-11-11 TS_OK SIGN=119C2E848DF2
```

