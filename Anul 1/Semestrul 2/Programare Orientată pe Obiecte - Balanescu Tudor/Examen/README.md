# Examen POO - Referinte

- [Examen POO - Referinte](#examen-poo---referinte)
    - [A. Operator overloading, C++](#a-operator-overloading-c)
    - [B. Threads, Java](#b-threads-java)
    - [C. Parameters passing, by ref, by value](#c-parameters-passing-by-ref-by-value)
    - [D. Late binding vs early binding](#d-late-binding-vs-early-binding)
    - [E. Static variables and methods, private constructor](#e-static-variables-and-methods-private-constructor)
    - [F. Fragile Base Class problem](#f-fragile-base-class-problem)
    - [G. Model View Controller pattern](#g-model-view-controller-pattern)
    - [H. Lexical scope, constructors, destructors](#h-lexical-scope-constructors-destructors)
    - [I. Interface, Java](#i-interface-java)
    - [J. Assign operator overloading, C++](#j-assign-operator-overloading-c)

### A. Operator overloading, C++

[Operator Overloading in C++](https://www.geeksforgeeks.org/operator-overloading-c/)

[Youtube - OPERATORS and OPERATOR OVERLOADING in C++](https://youtu.be/mS9755gF66w)

### B. Threads, Java

[Multithreading in Java](https://www.geeksforgeeks.org/multithreading-in-java/)

[Synchronized in Java](https://www.geeksforgeeks.org/synchronized-in-java/)

[Monitors in Process Synchronization](https://www.geeksforgeeks.org/monitors-in-process-synchronization/)

[Inter-thread Communication in Java - **wait(), notify() and notifyAll()**](https://www.geeksforgeeks.org/inter-thread-communication-java/)

[Youtube - Threads Part1 (9:43)](https://youtu.be/F-CkaU8aQZI) ; [Code](http://www.newthinktank.com/2012/02/java-video-tutorial-17/)

[Youtube - Threads Part2 (10:43)](https://www.youtube.com/watch?v=G2Xd5avyk_0) ; [Code](http://www.newthinktank.com/2012/02/java-video-tutorial-18/)

### C. Parameters passing, by ref, by value

[Parameter Passing Techniques in C/C++](https://www.geeksforgeeks.org/parameter-passing-techniques-in-c-cpp/)

### D. Late binding vs early binding

[Diferente](https://stackoverflow.com/questions/10580/what-is-the-difference-between-early-and-late-binding)

[Static vs Dynamic Binding in Java](https://www.geeksforgeeks.org/static-vs-dynamic-binding-in-java/)

[Early binding and Late binding in C++](https://www.geeksforgeeks.org/early-binding-late-binding-c/)

### E. Static variables and methods, private constructor

[Static keyword in java](https://www.geeksforgeeks.org/static-keyword-java/)

[Static Variables in C](https://www.geeksforgeeks.org/static-variables-in-c/)

### F. Fragile Base Class problem

[Explicatie scurta](https://stackoverflow.com/questions/2921397/what-is-the-fragile-base-class-problem)
[Articol mai cuprinzator](https://www.infoworld.com/article/2073649/why-extends-is-evil.html)

### G. Model View Controller pattern

[MVC Design Pattern](https://www.geeksforgeeks.org/mvc-design-pattern/)

[Youtube - MVC Java Tutorial (13:15)](https://www.youtube.com/watch?v=dTVVa2gfht8&t=6s)

### H. Lexical scope, constructors, destructors

[Lexical scope](https://stackoverflow.com/questions/1047454/what-is-lexical-scope)
**Constructors & Destructors**:

- C++: https://www.geeksforgeeks.org/constructors-c/
      https://www.geeksforgeeks.org/destructors-c/
- Java: https://www.geeksforgeeks.org/constructors-in-java/

### I. Interface, Java

[Interfaces in Java](https://www.geeksforgeeks.org/interfaces-in-java/)

[Youtube - Interfaces & Abstracts (8:29)](https://www.youtube.com/watch?v=1PPDoAKbaNA); [Code](http://www.newthinktank.com/2012/02/java-video-tutorial-15/)

### J. Assign operator overloading, C++

[Assignment Operators Overloading in C++](https://www.tutorialspoint.com/cplusplus/assignment_operators_overloading.htm) 