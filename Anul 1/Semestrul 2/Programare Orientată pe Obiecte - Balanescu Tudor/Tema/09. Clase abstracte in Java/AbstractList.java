public abstract class AbstractList {
    public abstract void add(AbstractElem e);
    public abstract AbstractElem item();
}