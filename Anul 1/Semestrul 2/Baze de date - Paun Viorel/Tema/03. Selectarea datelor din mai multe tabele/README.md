Exersati comanda Select pentru selectarea datelor din mai multe tabele (cf. exemplelor din note de curs si laborator) utilizand tabelele din propria dvs  baza de date.

Puneti in evidenta toate tipurile de asociere: inner join,  left join, right join, full join, cross join


Observatie:

Daca au aparut erori la  inserarea datelor  in tabelele bazei dvs. de date, cel mai probabil se datoreaza faptului ca modelul relational nu este corect.

In aceasta situatie:

-Studiati cu atentie modelul conceptual si relational al bazelor de date.

-Refaceti structura bazei de date. 
