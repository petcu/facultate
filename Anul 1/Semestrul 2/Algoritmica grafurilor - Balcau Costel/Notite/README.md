# Notiţe

- [Notiţe](#notiţe)
    - [BF - Breadth First (Parcurgerea pe nivele)](#bf---breadth-first-parcurgerea-pe-nivele)
    - [DF - Depth First (Parcurgerea în adâncime)](#df---depth-first-parcurgerea-în-adâncime)
    - [Algoritmul Dijkstra](#algoritmul-dijkstra)
    - [Grad Noduri](#grad-noduri)
    - [Graf Eulerian](#graf-eulerian)
    - [Graf Hemiltonian](#graf-hemiltonian)
    - [Graf Orientat](#graf-orientat)
    - [Graf Neorientat](#graf-neorientat)
    - [Algoritmii lui Kruskal şi Prim](#algoritmii-lui-kruskal-şi-prim)
    - [Matrice de Incidenta](#matrice-de-incidenta)
    - [Matricea Drumurilor](#matricea-drumurilor)
    - [Numar Ciclomatic](#numar-ciclomatic)

### BF - Breadth First (Parcurgerea pe nivele)

![BF](./img/BF.png)

![BF_exemplu](./img/BF_exemplu.png)

### DF - Depth First (Parcurgerea în adâncime)

![DF](./img/DF.png)

![DF_exemplu](./img/DF_exemplu.png)

### Algoritmul Dijkstra

![dijkstra](./img/dijkstra.png)

![dijkstra_exemplu1](./img/dijkstra_exemplu1.png)

![dijkstra_exemplu2](./img/dijkstra_exemplu2.png)

### Grad Noduri

![grad_noduri](./img/grad_noduri.png)

![grad_noduri_exemplu](./img/grad_noduri_exemplu.png)

### Graf Eulerian

![graf_eulerian](./img/graf_eulerian.png)

![graf_eulerian_exemplu](./img/graf_eulerian_exemplu.png)

### Graf Hemiltonian

![graf_hemiltonian](./img/graf_hemiltonian.png)

### Graf Orientat

![graf_orientat_exemplu](./img/graf_orientat_exemplu.png)

### Graf Neorientat

![graf_neorientat_exemplu](./img/graf_neorientat_exemplu.png)

### Algoritmii lui Kruskal şi Prim

![kruskal_si_prim](./img/kruskal_si_prim.png)

![kruskal_exemplu](./kruskal_exemplu.png)

![prim_exemplu](./img/prim_exemplu.png)

### Matrice de Incidenta

![matrice_de_incidenta](./img/matrice_de_incidenta.png)

![matrice_de_incidenta_exemplu](./img/matrice_de_incidenta_exemplu.png)

### Matricea Drumurilor

![matricea_drumurilor](./img/matricea_drumurilor.png)

![matricea_drumurilor_exemplu](./img/matricea_drumurilor_exemplu.png)

### Numar Ciclomatic

![numar_ciclomatic](./img/numar_ciclomatic.png)

![numar_ciclomatic_exemplu](./img/numar_ciclomatic_exemplu.png)

