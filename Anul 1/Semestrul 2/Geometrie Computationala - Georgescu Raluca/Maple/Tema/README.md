# Tema Maple - Grupa 2

- [Tema Maple - Grupa 2](#tema-maple---grupa-2)
  - [Laborator 04](#laborator-04)
  - [Laborator 05](#laborator-05)
  - [Laborator 06](#laborator-06)
  - [Laborator 07](#laborator-07)
  - [Laborator 11](#laborator-11)
  - [Laborator 12](#laborator-12)

## Laborator 04

Pornind de la fişierul maple intitulat Laborator04, îl completaţi pentru toate exerciţiile din Seminarul04 şi din Cursul04.

## Laborator 05

Pornind de la fişierul maple intitulat Laborator05, îl completaţi pentru exerciţiile din Seminarul05. `Grupa 1, ex. 3c), 4c) ; Grupa 2, ex. 3d), 4d) ; Cei de la Mate ex. 3a), 4a)`.

## Laborator 06

Pornind de la fişierul maple intitulat Laborator06, îl completaţi pentru exerciţiile din Seminarul06. `Grupa 1, ex. b), f) ; Grupa 2, ex. d), g) ; Cei de la Mate ex. e)`.

## Laborator 07

Pornind de la fişierul maple intitulat Laborator07, îl completaţi pentru exerciţiile din Cursul07 `Grupa 1, ex. b) pag. 4; Grupa 2, ex. c), pag 4` şi din Seminarul07 `Grupa 1 ex. 6a) ; Grupa 2 ex. 6b) ; Cei de la Mate ex. 3b)`.

## Laborator 11

Pornind de la fişierul maple intitulat “Laborator11”, îl completaţi pentru încă 2 din exerciţiile din Seminarul11 `Grupa 1 ex. b), f) ; Grupa 2 ex. c), e) ; Cei de la Mate ex. c), d)`.

## Laborator 12

Pornind de la fişierul maple intitulat “Laborator12”, doar scrieţi conica şi desenaţi unul din exerciţiile din Seminarul12. 