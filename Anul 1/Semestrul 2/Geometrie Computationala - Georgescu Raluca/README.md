 # Cuprins

## Seminar 00

- Determinanţi
- Sisteme de ecuaţii liniare

## Curs 01

- Noţiuni de algebră liniară
- Spaţii vectoriale
- Subspaţii vectoriale

## Curs 02

- Bază şi dimensiune

## Curs 03

- Matricea de trecere de la o bază la alta
- Lema substituţiei

## Curs 04

- Aplicaţii liniare
- Nucleul şi imaginea unei aplicaţii liniare

## Curs 05

- Matricea asociată unui operator liniar
- Vectori şi valori proprii

## Curs 06

- Funcţionale liniare
- Funcţionale biliniare
- Funcţionale pătratice

## Curs 07

- Spaţii vectoriale euclidiene

## Curs 08

- Planul în spaţiu
- Ecuaţii ale planului în spaţiu

## Curs 09

- Dreapta în spaţiu

## Curs 10

- Transformări geometrice în plan
- Conice (Part 1)

## Curs 11

- Conice (Part 2)

## Curs 12

- Conice (Part 3)

## Curs 13

- Cuadrice

## Curs 14 + Seminar 14

- Recapitulare finală
- Modele de subiecte pentru examen

