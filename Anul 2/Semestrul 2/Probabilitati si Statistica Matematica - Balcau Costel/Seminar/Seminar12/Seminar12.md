 # Seminar 12

## Petculescu Mihai-Silviu

[TOC]

## Sondajul Statistic. Determinarea Intervalelor de Încredere

### 586

Un eşantion aleator de $80$ de observaţii a fost selectat dintr-o populaţie normal distribuită. În urma calculelor a rezultat valoarea medie în eşantion $x = 14.1$ şi abaterea medie pătratică $2.6$. Să se determine intervalul de încredere, garantat cu o probabilitate de $95\%$, pentru media colectivităţii generale ($\overline{x_0}$).

```R
> alfa = 0.05 ; sample_mean = 14.1 ; n = 80 ; sigma = sqrt(2.6)
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = sample_mean - critical_z * sigma / sqrt(n)
> b = sample_mean + critical_z * sigma / sqrt(n)
> interval = c(a, b)
> interval
[1] 13.74666 14.45334
```

### 587

Presupunem că dorim să determinăm proporţia absolvenţilor unui liceu care se vor înscrie în învăţământul superior. Numărul elevilor de clasa a XII–a din liceul supus analizei este $500$. Un eşantion de $30$ de elevi este selectat aleator fără revenire, iar rezultatele cercetării arată că $19$ elevi vor să se înscrie la o instituţie din învăţământul superior. Utilizând aceste informaţii, să se estimeze proporţia în întreaga colectivitate (p), pentru un nivel de încredere de 90%.

```R
> alfa = 0.10 ; N = 500 ; n = 30 ; w = 19 / 30
> s_w = sqrt( w * (1 - w) / n * (1 - n / N) )
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = w - critical_z * s_w;
> b = w + critical_z * s_w;
> interval = c(a, b);
> interval
[1] 0.4930253 0.7736413
```

