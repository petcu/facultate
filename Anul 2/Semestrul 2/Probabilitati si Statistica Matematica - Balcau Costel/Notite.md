# Notiţe Curs

[TOC]

### 2.6 Scheme probabiliste 
#### Schema binomială

![image-20210506101451641](.img/Notite/image-20210506101451641.png)

#### Probe Bernoulli

![image-20210506101731889](.img/Notite/image-20210506101731889.png)

#### Schema multinomială

![image-20210506101912506](.img/Notite/image-20210506101912506.png)

#### Schema binomială generatizată / Schema lui Poisson

![image-20210506102131112](.img/Notite/image-20210506102131112.png)

#### Schema hipergeometrică / Schema bilei neı̂ntoarse

![image-20210506102503008](.img/Notite/image-20210506102503008.png)

#### Schema hipergeometrică generalizată

![image-20210506102649592](.img/Notite/image-20210506102649592.png)

#### Observaţia 2.6.3

Luând $\large r = 2$ ı̂n **schema hipergeometrică generalizată**, regăsim **schema hipergeometrică**.

#### Exemplu 2.6.1

![image-20210506103107878](.img/Notite/image-20210506103107878.png)

#### Exemplu 2.6.2

![image-20210506103455789](.img/Notite/image-20210506103455789.png)

![image-20210506103931382](.img/Notite/image-20210506103931382.png)

![image-20210506104008590](.img/Notite/image-20210506104008590.png)

![image-20210506104033637](.img/Notite/image-20210506104033637.png)

#### Exemplu 2.6.3

![image-20210506104211874](.img/Notite/image-20210506104211874.png)

#### Exemplu 2.6.4

![image-20210506105239715](.img/Notite/image-20210506105239715.png)

![image-20210506105352879](.img/Notite/image-20210506105352879.png)

