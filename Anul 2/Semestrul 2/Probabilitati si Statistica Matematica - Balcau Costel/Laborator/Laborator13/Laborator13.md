# Laborator 13

## Petculescu Mihai-Silviu

[TOC]

## Statistica Inferenţială

### Legea normală, reprezentare grafică

Reprezentarea grafică a funcţiei de densitate normale standard ($\mu = 0,\ \sigma = 1$) se face din linia de comandă astfel:

```R
> t = seq(-6, 6, length = 400)
> f = 1 / sqrt(2*pi) * exp(-t^2/2)
> plot(t, f, type = "l", lwd = 1)
```

![lab13_01](./img/lab13_01.png)

```R
> normal_density = function(limit) {
    t = seq(-6, 6, length = 400)
    f = 1 / sqrt(2*pi) * exp(-t^2/2)
    plot(t, f, type = "l", lwd = 1)
  }
> normal_density(6)
```

### III. Intervale de încredere pentru media unei populaţii cu dispersia cunoscută

Durata vieţii unui tip de baterie urmează cu aproximaţie o lege normală cu dispersia de $9$ ore. Pentru un eşantion de $100$ de baterii se măsoară o medie de viaţă de $20$ de ore. Să se determine un interval de incredere de $90\%$ pentru media de viaţă a întregii populaţii.

```R
> alfa = 0.1 ; sample_mean = 20 ; n = 100 ; sigma = sqrt(9)
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = sample_mean - critical_z * sigma / sqrt(n)
> b = sample_mean + critical_z * sigma / sqrt(n)
> interval = c(a, b)
> interval
[1] 19.50654 20.49346
```

Rezultatul este intervalul $[19.50654,\ 20.49346]$.

### Aplicaţii

#### Exerciţiu 1

Se cauta un interval de incredere de $90%$ pentru media unei populaţii normale cu
dispersia cunoscuta $\sigma^2 = 100$. Pentru aceasta se utilizează un eşantion aleator simplu de $25$ de indivizi a cărui medie de selecţie (calculată) este $67.53$.

```R
> alfa = 0.1 ; sample_mean = 67.53 ; n = 25 ; sigma = 10
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = sample_mean - critical_z * sigma / sqrt(n)
> b = sample_mean + critical_z * sigma / sqrt(n)
> interval = c(a, b)
> interval
[1] 64.24029 70.81971

# pentru probabilitatea de 99
> alfa = 0.01
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = sample_mean - critical_z * sigma / sqrt(n)
> b = sample_mean + critical_z * sigma / sqrt(n)
> interval = c(a, b)
> interval
[1] 62.37834 72.68166
```

#### Exerciţiu 2

Într-o instituţie publică există un automat de cafea reglat în aşa fel încât cantitatea de
cafea dintr-un pahar urmează o lege normală cu deviaţia standard $\sigma = 0.5 oz$. Pentru un eşantion de $n = 50$ de pahare ales la întâmplare, se măsoară o medie a greutăţii pentru un pahar de $5 oz$. Să se determine un interval de încredere de $95\%$ pentru media de greutate a unui pahar de cafea.

```R
> alfa = 0.5 ; sample_mean = 5 ; n = 50 ; sigma = 0.5
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = sample_mean - critical_z * sigma / sqrt(n)
> b = sample_mean + critical_z * sigma / sqrt(n)
> interval = c(a, b)
> interval
[1] 4.952306 5.047694
```

#### Exerciţiu 3

Într-o încercare disperată de a concura General Electric, compania ACME introduce un nou tip de becuri. ACME fabrică iniţial $100$ de becuri a căror medie de viaţă măsurată este $1280$ de ore (deviaţia standard a populaţiei este $140$ de ore). Să se găsească un interval de încredere de $99\%$ pentru media de viaţă a becurilor.

```R
> alfa = 0.01 ; sample_mean = 1280 ; n = 100 ; sigma = 140
> critical_z = qnorm(1 - alfa/2, 0, 1)
> a = sample_mean - critical_z * sigma / sqrt(n)
> b = sample_mean + critical_z * sigma / sqrt(n)
> interval = c(a, b)
> interval
[1] 1243.938 1316.062
```

### IV. Intervale de Îıncredere pentru media unei populaţii cu dispersia necunoscută

O companie ce produce jucării doreşte să afle cât de interesante sunt produsele sale. $60$ de copiii dintr-un eşantion sunt rugaţi să răspundă cu o valoare între $0$ şi $5$ şi se determină o medie egala cu $3.3$, cu o deviaţie standard $s = 0.4$. Cât de interesante, în medie, sunt jucăriile companiei ($95%$ nivel de încredere)?

```R
> alfa = 0.05 ; sample_mean = 3.3 ; n = 60 ; s = 0.4
> se = s/sqrt(n)
[1] 0.05163978
> critical_t = qt(1 - alfa / 2, n - 1)
[1] 2.000995
> a = sample_mean - critical_t * se
> b = sample_mean + critical_t * se
> interval = c(a, b)
> interval
[1] 3.196669 3.403331
```

Rezultatul este intervalul $[3.19667,\ 3.40333]$

### Aplicaţii

#### Exerciţiu 1

196 de studenţi aleşi aleator au fost întrebaţi cât de mulţi bani au investit în cumpăraturi online săptămâna trecută. Media a fost calculată la $44.65\$$, cu o dispersie (a eşantionului) egală cu $s^2 = 2.25$. Calculaţi un interval de incredere de $99\%$ pentru media populaţiei (despre care se presupune că urmează o lege normală).

```R
> alfa = 0.01 ; sample_mean = 44.65 ; n = 196 ; s = sqrt(2.25)
> se = s/sqrt(n)
> critical_t = qt(1 - alfa / 2, n - 1)
> a = sample_mean - critical_t * se
> b = sample_mean + critical_t * se
> interval = c(a, b)
> interval
[1] 44.37129 44.92871
```

#### Exerciţiu 2

O companie de dulciuri consideră ca nivelul de zahăr în produsele sale poate avea valori între $1$ si $20$, urmând o lege normală. Se consideră un eşantion de $49$ de produse. Media nivelului de zahăr este $12$, iar deviaţia standard a eşantionului este de $1.75$.

a. Determinaţi intervalele de încredere de $99\%$ şi $95\%$ pentru media nivelului de zahăr.

b. După modificarea reţetei, s-au testat $49$ produse şi s-a gşsit că media nivelului de zahăr este de $13.5$ cu o deviaţie standard de $1.25$. Determinaţi un interval de încredere de $95\%$ pentru media nivelului de zahăr.

```R
# a
## pentru 99%
> alfa = 0.01 ; sample_mean = 12 ; n = 49 ; s = 1.75
> se = s/sqrt(n)
> critical_t = qt(1 - alfa / 2, n - 1)
> a = sample_mean - critical_t * se
> b = sample_mean + critical_t * se
> interval = c(a, b)
> interval
[1] 11.32945 12.67055

## pentru 95%
> alfa = 0.05 ; sample_mean = 12 ; n = 49 ; s = 1.75
> se = s/sqrt(n)
> critical_t = qt(1 - alfa / 2, n - 1)
> a = sample_mean - critical_t * se
> b = sample_mean + critical_t * se
> interval = c(a, b)
> interval
[1] 11.49734 12.50266
```

```R
# b
> alfa = 0.05 ; sample_mean = 13.5 ; n = 49 ; s = 1.25
> se = s/sqrt(n)
> critical_t = qt(1 - alfa / 2, n - 1)
> a = sample_mean - critical_t * se
> b = sample_mean + critical_t * se
> interval = c(a, b)
> interval
[1] 13.14096 13.85904
```

### V. Testarea ipotezelor statistice - Testul z asupra proporţiilor

Un politician susţine că va primi mai puţin de $60\%$ dintre voturi în colegiul său. Un eşantion dintr-o $100$ de alegători arată ca $63$ dintre ei au votat pentru acest politician. Putem respinge afirmaţia politicianului? ($1\%$ nivel de semnificaţie)

```R
> alfa = 0.01 ; n = 100 ; succese = 63
> p_prim = succese/n
[1] 0.63
> p0 = 0.6
> z_score = (p_prim - p0) / sqrt(p0 * (1 - p0) / n)
[1] 0.6123724
> critical_z = qnorm(1 - alfa, 0, 1)
[1] 2.326348
```

Rezultatul este $z = 0.61237 < z∗ = 2.32634$, deci ipoteza nulă nu se poate respinge.



