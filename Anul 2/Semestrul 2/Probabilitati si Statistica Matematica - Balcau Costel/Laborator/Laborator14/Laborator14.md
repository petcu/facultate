# Laborator 14

## Petculescu Mihai-Silviu

[TOC] 

## Statistica Inferenţială

### I. Inferenţa asupra mediei

Un producător de becuri doreşte să testeze cu $5\%$ nivel de semnificaţie afirmaţia că media de viaţă a acestora este de cel puţin $810$ de ore (se ştie că deviaţia standard a
populaţiei este $\sigma = 50$ de ore). Se alege un eşantion de $200$ de becuri a căror medie de viaţă este găsită $816$ ore. Poate fi acceptată ipoteza producătorului?

```R
> alfa = 0.05 ; population_mean = 810 ; sample_mean = 816 ; n = 200
> sigma = 50
> critical_z = qnorm(1 - alfa)
> critical_z
[1] 1.644854
> z_score = (sample_mean - population_mean) / (sigma/sqrt(n))
> z_score
[1] 1.697056
```

Scorul va fi $z = 1.69705 > z^* = 1.64485$ şi ipoteza nulă poate fi respinsă, se acceptă ipoteza că media populaţiei este mai mare decat $810$.

### Aplicaţii

#### Exerciţiu 1

Dintr-o populaţie normală cu dispersia $\sigma^2 = 144$ se selectează $49$ de indivizi a căror medie este $88$. Să se testeze ipoteza că media populaţiei este mai mică decât $90$.

```R
> alfa = 0.01 ; population_mean = 90 ; sample_mean = 88 ; n = 49
> sigma = sqrt(144)
> critical_z = qnorm(1 - alfa)
> critical_z
[1] 2.326348
> z_score = (sample_mean - population_mean) / (sigma/sqrt(n))
> z_score
[1] -1.166667
```

Scorul va fi $z = -1.166667 < z^* = 2.326348$ şi ipoteza nulă nu poate fi respinsă.

#### Exerciţiu 2

Din experienţă se ştie că rezultatele studenţilor la un test de matematică urmează o lege normală cu media $75$ şi dispersia $17$. Catedra de matematică doreşte să afle dacă studenţii din anul curent au un comportament atipic. Media rezultatelor unui grup de $36$ de studenţi este $85$ de puncte. Cu $1\%$ nivel de semnificaţie se poate trage concluzia că studenţii din anul curent sunt atipici?

```R
> alfa = 0.01 ; population_mean = 75 ; sample_mean = 85 ; n = 36
> sigma = 17
> critical_z = qnorm(1 - alfa)
> critical_z
[1] 2.326348
> z_score = (sample_mean - population_mean) / (sigma/sqrt(n))
> z_score
[1] 3.529412
```

Scorul va fi $z = 3.529412 > z^* = 2.326348$ şi ipoteza nulă poate fi respinsă.

### II. Inferenţa asupra mediei - Testul t pentru media unei populaţii cu dispersia necunoscută

Pentru un experiment asupra metabolismului $5$ insecte sunt hrănite cu zahăr. Valorile nivelului de glucoză (care urmează o lege normală) obţinute din măsurători sunt:
$$
55.95\ 68.24\ 52.73\ 21.5\ 23.78
$$
Să se testeze cu $5\%$ nivel de semnificaţie ipoteza că media nivelului de glucoză este mai mare de $40$.

```R
> alfa = 0.05 ; population_mean = 40 ; n = 5
> x = c(55.95, 68.24, 52.73, 21.5, 23.78)
> sample_mean = mean(x) ; s = sd(x) ; se = s/sqrt(n)
> critical_t = qt(1 - alfa, n - 1)
> critical_t
[1] 2.131847
> t_score = (sample_mean - population_mean) / se
> t_score
[1] 0.4786769
```

Rezultatul va fi $t^∗ = 2.13184 > t = 0.47867$, ipoteza nulă nu poate fi respinsă.

#### Aplicaţie

Se măsoară pentru un eşantion provenit dintr-o populaţie normală următoarele valori
$$
36\ 32\ 28\ 33\ 41\ 28\ 31\ 26\ 29\ 34
$$
Cu $1\%$ nivel de semnificaţie să se testeze ipoteza că media are o valoare diferită de $34$.

```R
> alfa = 0.01 ; population_mean = 34 ; n = 10
> x = c(36, 32, 28, 33, 41, 28, 31, 26, 29, 34)
> sample_mean = mean(x) ; s = sd(x) ; se = s/sqrt(n)
> critical_t = qt(1 - alfa, n - 1)
> critical_t
[1] 2.821438
> t_score = (sample_mean - population_mean) / se
> t_score
[1] -1.557366
```

Rezultatul va fi $t^∗ = 2.821438 > t = -1.557366$, ipoteza nulă nu poate fi respinsă. Media are o valoare diferită de $34$.

### III. Inferenţa asupra mediei - Testul Z pentru diferenţa mediilor unor populaţii cu dispersii cunoscute

Se compară durata de viaţă a două tipuri de baterii. Primul tip are o deviaţie standard de $4$ ore, al doilea tip are o deviaţie standard de $3$ ore. Se aleg două eşantioane fiecare de dimensiune de $100$ de baterii. Pentru primul eşantion media de viaţă este de $48$ de ore, iar pentru cel de-al doilea de $47$ de ore. Să se testeze diferenţa mediilor de viaţă cu $5\%$ nivel de semnificaţie.

**Observaţie**. Testarea diferenţei mediilor echivalează cu o ipoteză alternativă de tipul $\mu_1 - \mu_2 \neq 0 = m_0$.

```R
> alfa = 0.05 ; m0 = 0 ; sample1_mean = 48 ; sample2_mean = 47
> n1 = 100 ; n2 = 100 ; sigma1 = 4 ; sigma2 = 3
> combined_sigma = sqrt(sigma1^2/n1 + sigma2^2/n2)
> critical_z = qnorm(1 - alfa/2)
> critical_z
[1] 1.959964
> z_score = (sample1_mean - sample2_mean - m0) / combined_sigma
> z_score
[1] 2
```

Rezultatul va fi $|z^*| = 1.95996 < |z| = 2.00$, ipoteza nulă va fi respinsă şi se acceptă ca mediile celor două populaţii sunt diferite.

### IV. Inferenţa asupra dispersiilor a două populaţii - Testul F

Rezultatele unui test psihologic efectuat pe două eşantioane, unul de femei şi
unul de barbaţi sunt următoarele:

- barbaţi: $n_1 = 120,\ s_1 = 5.05$
- femei: $n_2 = 135,\ s_2 = 5.44$

Se poate trage concluzia că dispersiile celor două populaţii diferă semnificativ ($1\%$)?

```R
> alfa = 0.01 ; n1 = 120 ; n2 = 135 ; s1 = 5.05 ; s2 = 5.44
> critical_F_s= qf(alfa/2, n1 - 1, n2 - 1)
> critical_F_s
[1] 0.6284359
> critical_F_d = qf(1 - alfa/2, n1 - 1, n2 - 1)
> critical_F_d
[1] 1.582571
> F_score = s1^2 / s2^2
> F_score
[1] 0.8617573
```

Scorul este $F = 0.86175$, valorile critice sunt $F^*_s = 0.62843$ şi $F^*_d = 1.58257$; deoarece $F \in [F^*_s, F^*_d]$ ipoteza nulă nu poate fi respinsă. În acest caz putem considera că nu există dovezi semnificative pentru a afirma că dispersiile sunt diferite.

#### Aplicaţie

Cercetătorii studiază amplitudinea mişcării obţinută prin stimularea nervoasă a şoarecilor.

- Pentru şoarecii drogaţi se obţin următoarele date: $12.512\ 12.869\ 19.098\ 15.350\ 13.297\ 15.589$
- Pentru şoarecii normali se obţin următoarele date: $11.074\ 9.686\ 12.164\ 8.351\ 12.182\ 11.489$

Influenţa drogurilor este semnificativă în ceea ce priveşte cele două dispersii ($5\%$ nivel de semnificaţie)?

```R
> alfa = 0.05 ; n1 = 6 ; n2 = 6
> x1 = c(12.512, 12.869, 19.098,15.350, 13.297, 15.589)
> x2 = c(11.074, 9.686, 12.164, 8.351, 12.182, 11.489)
> s1 = mean(x1) ; s2 = mean(x2)
> critical_F_s = qf(alfa/2, n1 - 1, n2 - 1)
> critical_F_s
[1] 0.139931
> critical_F_d = qf(1 - alfa/2, n1 - 1, n2 - 1)
> critical_F_d
[1] 7.146382
> F_score = s1^2 / s2^2
> F_score
[1] 1.865904
```

Scorul este $F = 1.865904$, valorile critice sunt $F^*_s = 0.139931$ şi $F^*_d = 7.146382$; deoarece $F \in [F^*_s, F^*_d]$ ipoteza nulă nu poate fi respinsă. În acest caz putem considera că nu există dovezi semnificative pentru a afirma că dispersiile sunt diferite.

### V. Inferenţa asupra mediilor a două populaţii - Testul T pentru diferenţa mediilor unor populaţii cu dispersii necunoscute

Rezultatele unui test psihologic efectuat pe două eşantioane, unul de femei şi
unul de barbaţi sunt următoarele:

- barbaţi: $n_1 = 110,\ \overline{x}_1 = 25.84,\ s_1 = 4.25$
- femei: $n_2 = 105,\ \overline{x}_1 = 21.53,\ s_2 = 3.85$

Se poate trage concluzia că mediile celor două populaţii diferă semnificativ ($1\%$)?
**Observaţie**: Testarea diferenţei mediilor echivalează cu o ipoteză alternativă de tipul $\mu_1 - \mu_2 \neq 0 = m0.$

```R
> alfa = 0.01 ; m0 = 0 ; n1 = 110 ; n2 = 105 ; s1 = 4.25 ; s2 = 3.85
> sample1_mean = 25.84 ; sample2_mean = 21.53
> critical_F_s = qf(alfa/2, n1 - 1, n2 - 1) # testul F
> critical_F_s
[1] 0.6054185
> critical_F_d = qf(1 - alfa/2, n1 - 1, n2 - 1)
> critical_F_d
[1] 1.656
> F_score = s1^2 / s2^2
> F_score
[1] 1.218587
> if(F_score < critical_F_s || F_score > critical_F_d) {
    df = min(n1 - 1, n2 - 1);
    combined_s = sqrt(s1^2/n1 + s2^2/n2);
  } else {
    df = n1 + n2 - 2;
    combined_s = sqrt( ( (n1-1)*s1^2 + (n2-1)*s2^2 ) / df ) * sqrt(1/n1 + 1/n2);
  }
> critical_t = qt(1 - alfa/2, df)
> critical_t
[1] 2.599108
> t_score = (sample1_mean - sample2_mean - m0) / combined_sigma
> t_score
[1] 8.62
```

Rezultatul va fi $|t^*| = 2.599108 > |t| = 8.62$, ipoteza nulă poate fi respinsă.

