# Capitolul 08 - Regresie liniară

[TOC]

## 8.1 Metoda celor mai mici pătrate
![image-20210606182533152](img/Capitolul08/image-20210606182533152.png)

![image-20210606182753042](img/Capitolul08/image-20210606182753042.png)

![image-20210606185000976](img/Capitolul08/image-20210606185000976.png)

![image-20210606185033764](img/Capitolul08/image-20210606185033764.png)

### Exemplu 1

![image-20210606182908946](img/Capitolul08/image-20210606182908946.png)

![image-20210606183544477](img/Capitolul08/image-20210606183544477.png)

![image-20210606183918408](img/Capitolul08/image-20210606183918408.png)

![image-20210606184701020](img/Capitolul08/image-20210606184701020.png)

<img src="img/Capitolul08/image-20210606184748060.png" alt="image-20210606184748060"  />

![image-20210606184846450](img/Capitolul08/image-20210606184846450.png)

