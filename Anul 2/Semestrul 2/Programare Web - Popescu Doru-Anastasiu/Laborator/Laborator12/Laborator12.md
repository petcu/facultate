# Laborator 12

## Petculescu Mihai-Silviu

[TOC]

## Javascript. Modul de lucru cu clasa `String`

### Exemplul 1

```html
<html>
  <body>
    <h3>Obiectul String. Determinarea lungimii unui sir</h3>
    <br/>
    <script>
      var txt = "Bine ati venit!";
      document.write(`Sirul este: ${txt} <br/>`);
      document.write(`Are lungimea ${txt.length}`);
    </script>
    <p><b>Obs.</b> Sirul nu se modifica.</p>
  </body>
</html>
```

![lab12_string_ex1](./img/lab12_string_ex1.png)

### Exemplul 2

```html
<html>
  <body>
    <h3>Obiectul String. Utilizarea tagurilor HTML pentru stilizarea unui sir.</h3>
    <br/>
    <script>
      var txt = "Bine ati venit!";
      document.write(`<p>Big: ${txt.big()} </p>`);
      document.write(`<p>Small: ${txt.small()} </p>`);
      document.write(`<p>Bold: ${txt.bold()} </p>`);
      document.write(`<p>Italic: ${txt.italics()} </p>`);
      document.write(`<p>Blink: ${txt.blink()} (nu functioneaza in IE, Chrome, Safari) </p>`);
      document.write(`<p>Fixed: ${txt.fixed()} </p>`);
      document.write(`<p>Strike: ${txt.strike()} </p>`);
      document.write(`<p>Fontcolor: ${txt.fontcolor("Blue")} </p>`);
      document.write(`<p>Fontsize: ${txt.fontsize(14)} </p>`);
      document.write(`<p>Subscript: ${txt.sub()} </p>`);
      document.write(`<p>Superscript: ${txt.sup()} </p>`);
      document.write(`<p>Link: ${txt.link("http://www.google.com")} </p>`);
    </script> 
    <br/><br/>
    <p><b>Obs.</b>Sirul stilizat nu se modifica!</p>
  </body>
</html>
```

![lab12_string_ex2](./img/lab12_string_ex2.png)

### Exemplul 3

Ilustrează cum se utilizează metoda `indexOf()` pentru a determina poziţia primei apariţii a unei valori
într-un şir.

```html
<html>
  <body>
    <h3>Obiectul String. Cautarea primei aparitii a unei valori in sir cu indexof().</h3>
    <br/>
    <script>
      var str = "Buna ziua!";
      document.write(`Sirul in care se cauta este:  ${str} <br/>`);
      document.write(`Sirul \"Buna\" apare in sir in pozitia ${str.indexOf("Buna")} <br/>`);
      document.write(`Sirul \"ZIUA\" apare in sir in pozitia ${str.indexOf("ZIUA")} <br/>`);
      document.write(`Sirul \"ziua\" apare in sir in pozitia ${str.indexOf("ziua")}`);
    </script>
    <p><b>Obs.</b> Sirul nu se modifica in urma cautarii!</p>
  </body>
</html>
```

![lab12_string_ex3](./img/lab12_string_ex3.png)

## Javascript. Modul de lucru cu obiecte `Array`

### Exemplu 1

Următorul exemplu foloseşte o instrucţiune `for` care parcurge un tablou (aici tabloul `nume_colegi`),
extrage valoarea fiecărui element şi o afişează.

```html
<html>
  <body>
    <script>
      var nume_colegi = new Array();
      nume_colegi[0] = "Cristi";
      nume_colegi[1] = "Ion";
      nume_colegi[2] = "Simona";
      nume_colegi[3] = "Adi";

      document.write("Afiseaza numele fiecarui coleg: <br/>");
      for (i = 0; i < nume_colegi.length; i++) {
        document.write(nume_colegi[i] + "<br/>");
      }
    </script>
  </body>
</html>
```

![lab12_array_ex1](./img/lab12_array_ex1.png)

### Exemplu 2

Folosim acelasi exemplu de mai sus.
"sort()" sorteaza elementele din Array in ordine alfabetica (sau crescatoare in cazul valorilor numerice)

```html
<html>
  <body>
    <script>
      var nume_colegi = new Array();
      nume_colegi[0] = "Cristi";
      nume_colegi[1] = "Ion";
      nume_colegi[2] = "Simona";
      nume_colegi[3] = "Adi";
      nume_colegi.sort();
        
      // Aranjeaza elementele in ordine crescatoare, alfabetic
      document.write("Afiseaza numele in ordine alfabetica: <br/>");
      for (i = 0; i < nume_colegi.length; i++) {
        document.write(nume_colegi[i] + "<br/>");
      }
    </script>
  </body>
</html>
```

![lab12_array_ex2](./img/lab12_array_ex2.png)

## Aplicaţii

### Exerciţiu 2

Memoraţi numele şi adresa de email pentru 5 persoane în obiecte `String`. Apoi afişaţi într-un
tabel cu trei coloane aceste informaţii (`nr.crt` / `nume` / `email`).

```html
<html>
  <head>
    <style>
      table { width: 50%; }
      table, th, td { border: 1px solid black; }
    </style>
  </head>
  <body>
    <script>
      let date = Array();
      date.push(["Name1", "name1@email.com"]);
      date.push(["Name2", "name2@email.com"]);
      date.push(["Name3", "name3@email.com"]);
      date.push(["Name4", "name4@email.com"]);
      date.push(["Name5", "name5@email.com"]);
      document.write("<table> <tr> <th> Nr. Crt </th> <th> Nume </th> <th> Mail </th> </tr>");
      date.forEach((e, index) => {
        document.write("<tr>");
        document.write(`<td> ${index} </td>`);
        document.write(`<td> ${e[0]} </td>`);
        document.write(`<td> ${e[1]} </td>`);
        document.write("</tr>");
      });
      document.write("</table>");
    </script>
  </body>
</html>
```

![lab12_02](./img/lab12_02.png)

### Exerciţiu 3

Pentru un şir de caractere care conţine două cuvinte separate printr-un spaţiu memorat într-un
obiect `String` afişaţi cuvintele pe rânduri diferite.

```html
<html>
  <body>
    <script>
      let text = "Carpe Diem";
      let temp = text.split(" ");
      for (let i = 0; i < temp.length; i++) document.write(`${temp[i]} <br>`);
    </script>
  </body>
</html>
```

![lab12_03](./img/lab12_03.png)

### Exerciţiu 4

Pentru un text dat ştergeţi din el spaţiile inutile dintre cuvinte.

```html
<html>
  <body>
    <script>
      let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus. Vel pretium lectus quam id. Dictum sit amet justo donec enim. '   ' Commodo ullamcorper a lacus vestibulum sed arcu non odio.";
      while (text.indexOf("  ") > 0) {
        text = text.replace("  ", " ");
      }
      document.write(text);
    </script>
  </body>
</html>
```

![lab12_04](./img/lab12_04.png)

### Exerciţiu 5

Creaţi un vector cu primele `n` numere impare. Apoi afişaţi numerele prime în ordine crescătoare,
respectiv descrecătoare parcurgând acest vector.

```html
<html>
  <body>
    <script>
      function nrPrim(x) {
        if (x < 2) return false;
        if (x == 2) return true;
        if (x % 2 == 0) return false;
        for (let i = 3; i < x / 3; i = i + 2) if (x % i == 0) return false;
        return true;
      }

      let n = 30;
      let date = Array();
      for (let i = 0; i < n; i++) date.push(i * 2 + 1);

      date = date.filter(nrPrim);
      document.write("Nr prime, ordine crescatoare <br>");
      date.forEach((e) => document.write(`${e} <br>`));

      document.write("<br> Nr prime, ordine descrescatoare <br>");
      date.sort((a, b) => b - a);
      date.forEach((e) => document.write(`${e} <br>`));
    </script>
  </body>
</html>
```

![lab12_05](./img/lab12_05.png)

### Exerciţiu 6

Creaţi un vector cu primele $30$ de elemente din şirul lui Fibonacci. Apoi afişaţi componentele pare pe o linie şi cele impare pe o altă linie.

```html
<html>
  <body>
    <script>
      function fibonacci(n) {
        let rez = Array();
        if (n < 0) return rez;
        rez.push(0);
        if (n == 1) return rez;
        rez.push(1);
        if (n == 2) return rez;
        for (let i = 2; i < n; i++) {
          let c = rez[i - 1] + rez[i - 2];
          rez.push(c);
        }
        return rez;
      }

      let n = 30;
      let date = fibonacci(n);
      document.write(`Fibonacci, primele ${n} elemente, pare: <br>`);
      date.forEach((e) => {
        if (e % 2 == 0) document.write(`${e}; `);
      });
      document.write(`<br> <br> Fibonacci, primele ${n} elemente, pare: <br>`);
      date.forEach((e) => {
        if (e % 2 == 1) document.write(`${e}; `);
      });
    </script>
  </body>
</html>
```

![lab12_06](./img/lab12_06.png)
