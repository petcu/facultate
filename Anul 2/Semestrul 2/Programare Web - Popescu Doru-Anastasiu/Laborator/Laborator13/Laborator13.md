# Laborator 13

## Petculescu Mihai-Silviu

[TOC] 

## Temă

### Exerciţiu 01

Creaţi un applet care să afişeze numele şi prenumele vostru împreună cu adresa de email.

```java
import java.applet.*;
import java.awt.*;

public class P01 extends Applet {

  private AppletContext context;
  private String nume, prenume, email;
  
  public void init(){
    context = this.getAppletContext();
    nume = this.getParameter("nume");
    prenume = this.getParameter("prenume");
    email = this.getParameter("email");
    if(nume == null)
        nume = "Nespecificat";
    if(prenume == null)
        prenume = "Nespecificat";
    if(email == null)
        email = "Nespecificat";
  }

  public void paint(Graphics g) {
    g.setFont(new Font("Arial", Font.BOLD, 16));
    g.drawString("Nume: " + nume, 10, 30);
    g.drawString("Prenume: " + prenume, 10, 60);
    g.drawString("Email: " + email, 10, 90);
  }
}
```

```html
<html>
  <applet code="P01.class" width="750" height="250">
    <param name="nume" value="Petculescu" />
    <param name="prenume" value="Mihai-Silviu" />
    <param name="email" value="petculescu25@gmail.com" />
  </applet>
</html>
```

![p01](./img/p01.png)

### Exerciţiu 02

Creaţi un applet care să afişeze o imagine şi sub ea ce reprezintă.

```java
import java.awt.*;
import java.applet.*;

public class P02 extends Applet {
  Image img;
  public void init() {
    img = getImage(getCodeBase(), "trei.jpg");
  }

  public void paint(Graphics g) {
    g.drawImage(img, 10, 10, this);
    g.drawOval(0, 420, 300, 50);
    g.setFont(new Font("Arial", Font.BOLD, 16));
    g.drawString("Cifra 3!", 120, 450);
  }
}
```

![p02](./img/p02.png)

### Exerciţiu 03

Folosind fire de execuţie creaţi un applet care să afişeze pătratele numerelor naturale mai mici sau egale cu $100$.

```java
import java.awt.*;
import java.applet.*;
import java.util.ArrayList;
import java.util.List;

public class P03 extends Applet {
  private String text = "";
  private int len = 60;

  public void start() {
    for (int i = 0; i <= 100; i++)
      text = text + " " + (i * i);
  }

  public void paint(Graphics g) {
    g.setFont(new Font("Arial", Font.BOLD, 16));
    List<String> rez = splitText(text, len);
    for (int i = 0; i < rez.size(); i++)
      g.drawString(rez.get(i), 10, (i + 1) * 30);
  }

  public static List<String> splitText(String src, int len) {
    List<String> output = new ArrayList<>();
    String temp = "";
    for (String word: src.split(" ")) {
      if (temp.length() > len) {
        output.add(temp);
        temp = "";
      }
      temp += word + " ";
    }
    if (temp.length() != 0)
      output.add(temp);
    return output;
  }
}
```

![p03](./img/p03.png)

### Exerciţiu 04

Folosind fire de execuţie creaţi un applet care să afişeze numerele superprime $<10000$.

```java
import java.awt.*;
import java.applet.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.applet.Applet;

public class P04 extends Applet implements Runnable {
  private final int N = 10000;
  private final int len = 5;
    
  private List<Integer> rez;
  private List<String> output;
  
  Thread appletThread = null;
  private boolean ok = false;

  public void init() {
    rez = new ArrayList<Integer>();
    startThread();
  }

  @Override
  public void run() {
    for (int i = 1; i <= N; i++)
      if (eSuperPrim(i))
        rez.add(i);
    ok = true;
  }

  public void paint(Graphics g) {
    if (ok == true) {
      g.setFont(new Font("Arial", Font.BOLD, 16));
      if (output == null)
        output = getText(rez, len);
      for (int i = 0; i < output.size(); i++)
        g.drawString(output.get(i), 10, 30 * (i + 1));
    }
  }

  private void startThread() {
    ok = false;
    appletThread = new Thread(this);
    appletThread.start();
  }

  public static List<String> getText(List<Integer> rez, int len) {
    List<String> output = new ArrayList<>();
    String temp = "";
    for (int i = 0; i < rez.size(); i++) {
      temp = String.format("%s %s", temp, rez.get(i));
      if ((i + 1) % len == 0) {
        output.add(temp);
        temp = "";
      }
    }
    if (temp.length() != 0)
      output.add(temp);
    return output;
  }

  public static boolean ePrim(int x) {
    if (x < 1) return false;
    if (x == 2) return true;
    if (x % 2 == 0) return false;
    for (int d = 3; d < x / 3; d += 2)
      if (x % d == 0)
        return false;
    return true;
  }

  public static boolean eSuperPrim(int x) {
    if (x < 10) return ePrim(x);
    while (x != 0) {
      if (ePrim(x) == false) return false;
      x = x / 10;
    }
    return true;
  }
}
```

![p04](./img/p04.png)

### Exerciţiu 05

Folosind fire de execuţie create un applet, care să afişeze fiecare literă mare din alfabet de atâtea ori cât este poziţia sa în alfabet.

```java
import java.awt.*;
import java.applet.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.applet.Applet;

public class P05 extends Applet implements Runnable {

  private static final String alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private List<String> output;

  Thread appletThread = null;
  private boolean ok = false;

  public void init() {
    output = new ArrayList<String>();
    startThread();
  }

  private void startThread() {
    ok = false;
    appletThread = new Thread(this);
    appletThread.start();
  }

  @Override
  public void run() {
    for (int i = 0; i < alfabet.length(); i++) {
      String temp = new String(new char[i + 1]).replace("\0", String.valueOf(alfabet.charAt(i)));
      output.add(temp);
    }
    ok = true;
  }

  public void paint(Graphics g) {
    if (ok == true) {
      g.setFont(new Font("Arial", Font.BOLD, 16));
      for (int i = 0; i < output.size(); i++)
        g.drawString(output.get(i), 10, 20 * (i + 1));
    }
  }
}
```

![p05](./img/p05.png)

### Exerciţiu 06

Folosind fire de execuţie creaţi un applet care să afişeze toate anagramele cuvântului **marine**.

```java
import java.awt.*;
import java.applet.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.applet.Applet;

public class P06 extends Applet implements Runnable {
  private AppletContext context;
  private List<String> output;
  private String text;
  private int len = 23;

  Thread appletThread = null;
  private boolean ok = false;

  public void init() {
    output = new ArrayList < String > ();
    context = this.getAppletContext();
    text = this.getParameter("text");
    if (text == null)
      text = "marine";
    startThread();
  }

  private void startThread() {
    ok = false;
    appletThread = new Thread(this);
    appletThread.start();
  }

  @Override
  public void run() {
    permutation("", text);
    ok = true;
  }

  public void paint(Graphics g) {
    if (ok == true) {
      g.setFont(new Font("Arial", Font.BOLD, 16));
      String temp = "";
      int j = 1;
      for (int i = 0; i < output.size(); i++)
        if ((i + 1) % len == 0) {
          g.drawString(temp, 10, 20 * j);
          j++; temp = "";
        } else
          temp = temp + output.get(i) + " ";
    }
  }

  private void permutation(String prefix, String str) {
    int n = str.length();
    if (n == 0) output.add(prefix);
    else
      for (int i = 0; i < n; i++)
        permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n));
  }
}
```

```html
<html>
  <applet code="P06.class" width="750" height="550">
    <param name="text" value="marine" />
  </applet>
</html>
```

![p06](./img/p06.png)

### Exerciţiu 07

Creaţi un applet care să conţină un pătrat şi o minge de culoare galbenă care să se deplaseze din colţul din dreapta sus în colţul din stânga jos.

```java
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class P07 extends Applet implements Runnable {
  private int x = 200, xChange = -5, y = 0, yChange = 5;
  private int diameter = 20;
  private int rectLeftX = 0, rectRightX = 200;
  private int rectTopY = 0, rectBottomY = 200;
  private boolean ok;
  
  public void intt() {
    ok = true;
  }
  
  public void paint(Graphics g) {
    super.paint(g);
    g.drawRect(rectLeftX, rectTopY, rectRightX - rectLeftX + 10, rectBottomY - rectTopY + 10);
    g.setColor(getBackground());
    g.fillOval(x, y, diameter, diameter);
    
    if (x + xChange <= rectLeftX || x + xChange >= rectRightX)
      xChange = 0;
    else if (y + yChange <= rectTopY || y + yChange >= rectBottomY)
      yChange = 0;
    else {
      x = x + xChange;
      y = y + yChange;
      g.setColor(Color.yellow);
      g.fillOval(x, y, diameter, diameter);
    }
  }

  public void destroy() { ok = false; }
  public void start() {
    ok = true;
    new Thread(this).start();
  }
  public void stop() { ok = false; }
  public void run() {
    while (ok) {
      repaint();
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {}
    }
  }
}
```

![p07](./img/p07.png)

### Exerciţiu 08

Creaţi un applet care să conţină un pătrat şi o minge de culoare roşie care să se deplaseze din colţul din dreapta jos în colţul din stânga sus.

```java
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class P08 extends Applet implements Runnable {
  private int x = 0, xChange = 5, y = 200, yChange = -5;
  private int diameter = 20;
  private int rectLeftX = 0, rectRightX = 200;
  private int rectTopY = 0, rectBottomY = 200;
  private boolean ok;
  
  public void intt() {
    ok = true;
  }
  
  public void paint(Graphics g) {
    super.paint(g);
    g.drawRect(rectLeftX, rectTopY, rectRightX - rectLeftX + 10, rectBottomY - rectTopY + 10);
    g.setColor(getBackground());
    g.fillOval(x, y, diameter, diameter);

    if (x + xChange <= rectLeftX || x + xChange >= rectRightX)
      xChange = 0;
    else if (y + yChange <= rectTopY || y + yChange >= rectBottomY)
      yChange = 0;
    else {
      x = x + xChange;
      y = y + yChange;
      g.setColor(Color.red);
      g.fillOval(x, y, diameter, diameter);
    }
  }

  public void destroy() { ok = false; }
  public void start() {
    ok = true;
    new Thread(this).start();
  }
  public void stop() { ok = false; }
  public void run() {
    while (ok) {
      repaint();
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {}
    }
  }
}
```

![p08](./img/p08.png)

### Exerciţiu 09

Creaţi un applet care să conţină un dreptunghi şi o elipsă de culoare roşie care să se deplaseze pe conturul dreptunghiului.

```java
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
public class P09 extends Applet implements Runnable {
  private int x = 0, xChange = 5, y = 0, yChange = 0;
  private int diameter = 20;
  private int rectLeftX = 0, rectRightX = 400;
  private int rectTopY = 0, rectBottomY = 200;
  private boolean ok;
  
  public void intt() {
    ok = true;
  }
  
  public void paint(Graphics g) {
    super.paint(g);
    g.drawRect(rectLeftX, rectTopY, rectRightX - rectLeftX + 10, rectBottomY - rectTopY + 10);
    g.setColor(getBackground());
    g.fillOval(x, y, diameter + 20, diameter);

    if (x + xChange <= rectLeftX) { //stanga jos
      xChange = 0;
      yChange = -5;
    }
    if (x + xChange + 20 >= rectRightX) { //dreapta sus
      xChange = 0;
      yChange = 5;
    }
    if (y + yChange <= rectTopY) { //stanga sus
      yChange = 0;
      xChange = 5;
    }
    if (y + yChange >= rectBottomY) { //dreapta jos
      yChange = 0;
      xChange = -5;
    }
    x = x + xChange;
    y = y + yChange;
    g.setColor(Color.red);
    g.fillOval(x, y, diameter + 20, diameter);
  }

  public void destroy() { ok = false; }
  public void start() {
    ok = true;
    new Thread(this).start();
  }
  public void stop() { ok = false; }
  public void run() {
    while (ok) {
      repaint();
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {}
    }
  }
}
```

![p09](./img/p09.png)

