import java.awt.*;
import java.applet.*;

public class P02 extends Applet {

  Image img;
  public void init() {
    img = getImage(getCodeBase(), "trei.jpg");
  }

  public void paint(Graphics g) { //g=suprafata pe care se poate desena
    g.drawImage(img, 10, 10, this);
    g.drawOval(0, 420, 300, 50);
    g.setFont(new Font("Arial", Font.BOLD, 16));
    g.drawString("Cifra 3!", 120, 450);
  }
}
