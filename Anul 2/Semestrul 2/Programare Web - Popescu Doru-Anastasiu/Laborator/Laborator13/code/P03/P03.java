import java.awt.*;
import java.applet.*;
import java.util.ArrayList;
import java.util.List;

public class P03 extends Applet {
  private String text = "";
  private int len = 60;

public void start() {
for(int i = 0; i<=100; i++)
    text = text + " " + (i*i);
}

  
  public void paint(Graphics g) {
    g.setFont(new Font("Arial", Font.BOLD, 16));
    List<String> rez = splitText(text, len);
    for(int i=0; i<rez.size(); i++)
      g.drawString(rez.get(i), 10, (i+1)*30);
  }
  
  public static List<String> splitText(String src, int len){
        List<String> output = new ArrayList<>();
        String temp = "";
        for(String word : src.split(" ")){
            if(temp.length() > len){
                output.add(temp);
                temp = "";
            }
            temp += word + " ";
        }
        if(temp.length() != 0)
            output.add(temp);
        return output;
    }
}
 
