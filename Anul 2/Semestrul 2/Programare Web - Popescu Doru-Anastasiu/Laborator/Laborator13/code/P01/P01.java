import java.applet.*;
import java.awt.*;
public class P01 extends Applet {

  private AppletContext context;
  private String nume, prenume, email;
  
  public void init(){
    context = this.getAppletContext();
    nume = this.getParameter("nume");
    prenume = this.getParameter("prenume");
    email = this.getParameter("email");
    
    if(nume == null)
        nume = "Nespecificat";
    if(prenume == null)
        prenume = "Nespecificat";
    if(email == null)
        email = "Nespecificat";
  }

  public void paint(Graphics g) {
    g.setFont(new Font("Arial", Font.BOLD, 16));
    g.drawString("Nume: " + nume, 10, 30);
    g.drawString("Prenume: " + prenume, 10, 60);
    g.drawString("Email: " + email, 10, 90);
  }
}
