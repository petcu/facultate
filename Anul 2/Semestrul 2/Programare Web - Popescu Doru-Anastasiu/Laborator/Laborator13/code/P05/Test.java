import java.awt.*;
import java.applet.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.applet.Applet;

public class Test extends Applet implements Runnable {
    
    private static final String alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private List<String> output;
    
    Thread appletThread = null;
    private boolean ok = false;

    public void init(){
        output = new ArrayList<String>();
        startThread();
    }
    
    private void startThread(){
        ok = false;
        appletThread = new Thread(this);
        appletThread.start();
    }

    @Override
    public void run() {
        for(int i=0; i<alfabet.length(); i++){
            String temp = new String(new char[i+1]).replace("\0",
            String.valueOf( alfabet.charAt(i)));
            output.add(temp);
        }
        ok = true;
    }
    
    public void paint(Graphics g){      
        if(ok == true){
            g.setFont(new Font("Arial", Font.BOLD, 16));
            for(int i = 0; i<output.size(); i++)
                g.drawString(output.get(i), 10, 20*(i+1));
        }
    }

}
