import java.awt.*;
import java.applet.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.applet.Applet;

public class Test extends Applet implements Runnable {
    
    private AppletContext context;
    private List<String> output;
    private String text;
    private int len = 23;
    
    Thread appletThread = null;
    private boolean ok = false;

    public void init(){
        output = new ArrayList<String>();
        context = this.getAppletContext();
        text = this.getParameter("text");
        if(text == null)
            text = "marine";
        startThread();
    }
    
    private void startThread(){
        ok = false;
        appletThread = new Thread(this);
        appletThread.start();
    }

    @Override
    public void run() {
        permutation("", text); 
        ok = true;
    }
    
    public void paint(Graphics g){      
        if(ok == true){
            g.setFont(new Font("Arial", Font.BOLD, 16));
            String temp = "";
            int j = 1;
            for(int i = 0; i < output.size(); i++)
                if((i+1) % len == 0){
                    g.drawString(temp, 10, 20 * j);
                    j++;
                    temp = "";
                } else {
                    temp = temp + output.get(i) + " ";
                }
            //for(int i = 0; i<output.size(); i++)
            //    g.drawString(output.get(i), 10, 20*(i+1));
        }
    }
    
    private void permutation(String prefix, String str) {
        int n = str.length();
        if (n == 0){
            output.add(prefix);
        
        } else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n));
        }
    }

}
