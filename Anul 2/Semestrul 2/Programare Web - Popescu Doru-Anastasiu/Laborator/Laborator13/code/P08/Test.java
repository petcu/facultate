import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
public class Test extends Applet implements Runnable {
    private int x = 0, xChange = 5;
    private int y = 200, yChange = -5;
    private int diameter = 20;
    private int rectLeftX = 0, rectRightX = 200;
    private int rectTopY = 0, rectBottomY = 200;
    private boolean ok;
    public void intt() {
        ok = true;
    }
    public void paint(Graphics g) {
        super.paint(g);
        g.drawRect(rectLeftX, rectTopY, rectRightX - rectLeftX + 10, rectBottomY - rectTopY + 10);
        /*desenam cu culoarea fundalului pentru a acoperi mingea anterior desenata*/
        g.setColor(getBackground());
        //se sterge mingea
        g.fillOval(x, y, diameter, diameter);
        
        if(x + xChange <= rectLeftX || x + xChange >= rectRightX)
            xChange = 0;
        else if(y + yChange <= rectTopY || y + yChange >= rectBottomY)
            yChange = 0;
        else {
            x = x + xChange;
            y = y + yChange;
            g.setColor(Color.red);
            g.fillOval(x, y, diameter, diameter);
        }

        //daca se intalneste peretele, se schimba directia de miscare
//         if (x + xChange <= rectLeftX)
//             xChange = -xChange;
//         if (x + xChange >= rectRightX)
//             xChange = -xChange;
//         if (y + yChange <= rectTopY)
//             yChange = -yChange;
//         if (y + yChange >= rectBottomY)
//             yChange = -yChange;
        //se modifica noua pozitie a mingii
//         x = x + xChange;
//         y = y + yChange;
        //se stabileste culoarea rosie pentru desenare
//         g.setColor(Color.yellow);
        //se deseneaza mingea
//         g.fillOval(x, y, diameter, diameter);
    }
    /*se apleleaza la distrugerea appletului
    si are ca efect terminarea metodei paint ()*/
    public void destroy() {
        ok = false;
    }
    
    public void start() {
        ok = true;
        new Thread(this).start();
    }
    
    public void stop() {
        ok = false;
    }
    
    public void run() {
        while (ok) {
            repaint();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }
}
