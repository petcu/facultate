import java.awt.*;
import java.applet.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.applet.Applet;

public class Test extends Applet implements Runnable {

    private final int N = 10000;
    private final int len = 5;
    private List<Integer> rez; 
    Thread appletThread = null;
    private boolean ok = false;
    
    
    private List<String> output;

    public void init(){
        rez = new ArrayList<Integer>();
        startThread();
    }

    @Override
    public void run() {
        for(int i=1; i <= N; i++)
            if(eSuperPrim(i))
                rez.add(i);
        ok = true;
    }
    
    public void paint(Graphics g){      
        if(ok == true){
            g.setFont(new Font("Arial", Font.BOLD, 16));
            if(output == null)
                output = getText(rez, len);
            for(int i = 0; i<output.size(); i++)
                g.drawString(output.get(i), 10, 30*(i+1));
        }
        
    }
    
    
    private void startThread(){
        ok = false;
        appletThread = new Thread(this);
        appletThread.start();
    }
    
    public static List<String> getText(List<Integer> rez, int len){
        List<String> output = new ArrayList<>();
        String temp = "";
        for(int i=0; i<rez.size(); i++){
            temp = String.format("%s %s", temp, rez.get(i));
            if((i+1) % len == 0){
                output.add(temp);
                temp = "";
            }
        }
        if(temp.length() != 0)
            output.add(temp);
        return output;
    }
    
    public static boolean ePrim(int x){
        if(x < 1)
            return false;
        if(x == 2)
            return true;
        if(x%2 == 0)
            return false;
        for(int d=3; d<x/3; d+=2)
            if(x%d == 0)
                return false;
        return true;
    }

    public static boolean eSuperPrim(int x){
        if(x < 10)
            return ePrim(x);
        while(x != 0){
            if(ePrim(x) == false)
                return false;
            x = x / 10;
        }
        return true;
    }
}
