import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
public class Test extends Applet implements Runnable {
    private int x = 0, xChange = 5;
    private int y = 0, yChange = 0;
    private int diameter = 20;
    private int rectLeftX = 0, rectRightX = 400;
    private int rectTopY = 0, rectBottomY = 200;
    private boolean ok;
    public void intt() {
        ok = true;
    }
    public void paint(Graphics g) {
        super.paint(g);
        g.drawRect(rectLeftX, rectTopY, rectRightX - rectLeftX + 10, rectBottomY - rectTopY + 10);
        /*desenam cu culoarea fundalului pentru a acoperi mingea anterior desenata*/
        g.setColor(getBackground());
        //se sterge mingea
        g.fillOval(x, y, diameter+20, diameter);
        
//         if(x + xChange <= rectLeftX || x + xChange >= rectRightX)
//             xChange = 0;
//         else if(y + yChange <= rectTopY || y + yChange >= rectBottomY)
//             yChange = 0;
//         else {
//             x = x + xChange;
//             y = y + yChange;
//             g.setColor(Color.yellow);
//             g.fillOval(x, y, diameter, diameter);
//         }

        //daca se intalneste peretele, se schimba directia de miscare
        if (x + xChange <= rectLeftX){ //stanga jos
            xChange = 0;
            yChange = -5;
        }
        if (x + xChange + 20 >= rectRightX){ //dreapta sus
            xChange = 0;
            yChange = 5;
        }
        if (y + yChange <= rectTopY){ //stanga sus
            yChange = 0;
            xChange = 5;
        }
        if (y + yChange >= rectBottomY){ //dreapta jos
            yChange = 0;
            xChange = -5;
        }

        x = x + xChange;
        y = y + yChange;
        g.setColor(Color.red);
        g.fillOval(x, y, diameter+20, diameter);

    }
    /*se apleleaza la distrugerea appletului
    si are ca efect terminarea metodei paint ()*/
    public void destroy() {
        ok = false;
    }
    
    public void start() {
        ok = true;
        new Thread(this).start();
    }
    
    public void stop() {
        ok = false;
    }
    
    public void run() {
        while (ok) {
            repaint();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }
}
