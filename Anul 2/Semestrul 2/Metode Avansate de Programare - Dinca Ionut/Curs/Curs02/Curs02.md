# Curs 02

 [TOC]

## Elemente generale de UML (Unified Modelling Language)

**UML**: limbaj de modelare (nu este limbaj de programare!) care permite analiza, modelarea, specificarea cerinţelor unui sistem software.

**Elemente grafice şi textuale.**

**Diagrame**:

- de cazuri de utilizare
- de clase
- de secvenţă
- de colaborare
- de stare
- de componente
- de activităţi

In general, un proces de dezvoltare a unui sistem software conţine următoarele etape (principale):

1. Analiza şi specificarea cerinţelor
2. Proiectare şi implementare software (design & code) 
3. Verificare şi testare
4. Mentenanţa.

**ANALIZA** şi **PROIECTARE** orientată pe obiecte.

Caracteristicile unui sistem sofware de calitate:
1. Extensibilitate (flexibilitate) - noi funcţionalităţi - costuri cât mai mici
2. Reutilizabil - sistemul propune soluţii generale, care pot fi (re)-utilizate şi în alte sisteme software
3. Robust - sistemul este bine testat, se comportă bine în situaţii limită
4. Mentenabil

Diagramă de cazuri de utilizare:
- indică utilizatorii şi cum interacţionează aceştia cu sistemul.
- actori
- cazuri de utilizare (scenarii particulare de utilizare a sistemului)

