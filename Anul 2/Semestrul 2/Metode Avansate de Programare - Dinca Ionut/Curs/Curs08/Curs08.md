# Curs 08

[TOC]

## Şabloane de proiectare creaţionale

- **Singleton Pattern**
- **Abstract Factory Pattern**
- **Factory Method Pattern**

### Factory Method Pattern

**Scenariu**: O clasă `Document` - o colecţie de obiecte de tip `Page`. Dorim să abstractizăm atât noţiunea de document, cât şi noţiuninea de pagină (în cadrul documentului) pentru a obţine un grad ridicat de flexibilitate / extensibilitate - să putem crea diverse tipuri de documente.

- **Document**

```c#
 public abstract class Document {
   public List<Page> Pages { get; private set; }
   public Document() {
     Pages = new List<Page>();
     CreatePages();
   }
   public void AddPage(Page page) {
     Pages.Add(page);
   }
   public void Print() {
     Pages.ForEach(p => p.Print());
   }
   protected abstract void CreatePages(); // Metoda factory          
 }
```

- **ReportDocument**

```c#
class ReportDocument: Document {
  protected override void CreatePages() {
    AddPage(new ContentPage());
    AddPage(new IntroductionPage());
    AddPage(new TextPage());
    AddPage(new BibliographyPage());
  }
}
```

- **ResumeDocument**

```c#
class ResumeDocument: Document {
  protected override void CreatePages() {
    AddPage(new IntroductionPage());
    AddPage(new EducationPage());
    AddPage(new ResultsPage());
  }
}
```

- **Page**

```c#
public abstract class Page {
  public abstract void Print();
}
```

- **BibliographyPage**

```c#
class BibliographyPage: Page {
  public override void Print() {
    Console.WriteLine("Bibilography.");
  }
}
```

- **Alt exemplu**

```c#
abstract class Creator {
  public A Component1 { get; private set; }
  public B Component2 { get; private set; }
  public C Component3 { get; private set; }
  public Creator() {
    Component1 = CreateComponent1();
    Component2 = CreateComponent2();
    Component3 = CreateComponent3();
  }
  protected abstract A CreateComponent1(); // Factory method
  protected abstract B CreateComponent2(); // Factory method
  protected abstract C CreateComponent3(); // Factory method
}

class ConcreteCreator: Creator {
  protected override A CreateComponent1() {
    return new ConcreteA();
  }
  protected override B CreateComponent2() {
    return new ConcreteB();
  }
  protected override C CreateComponent3() {
    return new ConcreteC();
  }
}

abstract class A {}
class ConcreteA: A {}

abstract class B {}
class ConcreteB: B {}

abstract class C {}
class ConcreteC: C {}
```

Şablonul `Factory Method` deleagă (în cadrul unei clase abstracte) claselor derivate crearea obiectelor. 

