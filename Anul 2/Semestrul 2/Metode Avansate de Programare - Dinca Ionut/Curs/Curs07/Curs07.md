# Curs 07

[TOC]

## Aplicații de tip Undo/Redo (multi-level) (Șablonul Command)

Pentru o aplicație cu interfață grafică (Agenda de Contacte), să se implementeze
mecanismul **undo/redo de nivel n**. Aplicația trece printr-o **suită de stări** provocată de o **suită de comenzi** efectuate!
$$
S_0 \Rightarrow S_1 \Rightarrow S_2\ ...\ S_n \Rightarrow S_{n+1}\\
C_0 \Rightarrow C_1 \Rightarrow C_2\ ...\ C_n \Rightarrow C_{n+1}
$$
**Executare `Undo`**: să anulez efectul comenzii $C_{n+1}$; aplicația va trece în starea $S_{n}$.

**Soluția**: pentru anularea unei comenzi se va memora cea mai mica informație posibilă: **diferența dintre starea curentă și cea anterioară**.

**Exemplu**: pentru $C_{n+1}$ memorez diferența dintre $S_{n+1}$ și $S_{n}$.

**Exemplu concret**: Dacă $C_{n+1}$ este: **adăugarea unui nou contact în agendă**!
adică voi memora noul contact adăugat și poziția pe care a fost adăugat!
$$
S_0 \Rightarrow S_1 \Rightarrow S_2\ ...\ S_i \Rightarrow S_{i+1}\ ...\ S_{n} \Rightarrow S_{n+1}\\
C_0 \Rightarrow C_1 \Rightarrow C_2\ ...\ C_{i}\ ...\ C_n \Rightarrow C_{n+1}
$$
Dacă avem $n$ comenzi executate la un moment dat, putem efectua `Undo` / `Redo`.

$$
C_{0} \Rightarrow C_{1} \Rightarrow C_{2}\ ...\ C_{i} \Rightarrow\ ...\ C_{n} \Rightarrow C_{n+1}\ -\ \mathrm{suita\ de\ comenzi\ efectuate}\\
\Leftarrow \mathrm{Undo}\ ...\Leftarrow \mathrm{Undo}\\
\mathrm{Redo} \Rightarrow
$$
Avem nevoie de o listă a ultimelor $n$ comenzi executate (`HistoryList`) - istoric al comenzilor. Cu ajutorul unui cursor, ne poziționăm pe comanda care urmează să fie anulată / refăcută!

1. **HistoryList**: istoric al comenzilor (listă cu cursor)
2. **Abstractizăm noțiunea de comandă** (de executat, de anulat, de refăcut)
3. **HistoryList** va conține comenzi abstracte

```c#
abstract class Command {
  public Receiver { get; } 
  // Obiectul asupra caruia se executa comanda (cel care trece prin stari succesive)
  // in cazul nostru, va fi AgendaGUI (interfata grafica pentru agenda de contacte)
  public string Name { get; }
  // Nume unic al comenzii
  public abstract void Execute();
  public abstract void Undo();
  // Metoda template
  public virtual void Redo() { Execute(); }
}

// Exemplu de comanda particulara:

class AddContact: Command {
  public ContactModel Contact { get; private set; }
  public int Position { get; private set; }
  public AddContact(int position, ContactModel contact, Receiver receiver) {
    Position = position;
    Contact = contact;
    Receiver = receiver;
  }
  public void Execute() {
    // Se adauga noul contact in baza de date se afiseaza
    // noua lista de contacte pe interfata (obiectul Receiver isi actualizeaza starea) - adaugare contact pe pozitia "Position"
  }
  public void Undo() {
    // Eliminare contact din baza de date
    // afisare noua lista de contacte (fara contactul eliminat)
  }
}
class RemoveContact: Commmand { }
class EditContact: Command { }
```

**Observaţie**: Dacă s-a executat cel puțin un `Undo` (cursorul nu mai este poziționat pe ultima comandă din listă) și se execută o nouă comandă (alta decât `undo` sau `redo`, care trebuie să ajungă în `HistoryList` pe ultima poziție, atunci toate comenzile din dreapta cursorului vor fi eliminate din lista - nu mai are sens să vorbim despre `Redo` pentru acestea.

```c#
public class HistoryList {
  static readonly int MAX_SIZE = 50; // Maxim al comenzilor
  List<Command> _commands;
  int _cursor;
  public HistoryList() {
    _commands = new List<Command>();
    _cursor = -1;
  }
  public void Add(Command newCommand) {
    // O noua comanda de executat se adauga in lista
    if (_commands.Count == MAX_SIZE) {
      // Efectuati deplasare la stanga a tuturor comenzilor cu o pozitie, astfel "cea mai veche comanda" se va pierde
    }
    if (_cursor < _commands.Count - 1) {
      // Avem cel putin un "Undo" elimin toate comenzile din dreapta cursorului!
      RemoveAllFromRight();
    }
    // Inserarea a noi comenzi pe ultima pozitie (alta varianta: lista circulara!!)                            
    _commands.Add(newCommand);
    _cursor = _commands.Count - 1; // Pozitionarea cursorului pe aceasta comanda
  }
  public void Remove(Command c) {
    // A se implementa o metoda Equals pentru obiectele comand dupa un camp cu o valoare unica (de exemplu numele comenzii) - unic sau un cod unic al comenzii (Guid)
  }
  public void RemoveAllFromRight() {
    // Eliminare comenzi din dreapta cursorului
  }
  public void Undo() {
    Item.Undo(); // Comanda de la pozitia cursorului este anulata!
    _cursor--; // Mutam cursorul la stanga
  }
  public Command Item {
    // Comanda de la pozitia cursorului
    get {
      return _commands[_cursor]; // Test _cursor!!
    }
  }
  public void Redo() {
    Item.Redo(); // Re-execut comanda de la pozitia cursorului
    _cursor++; // Mutam cursorul la dreapta
  }
  public bool BeforeFirst {
    // Not Undo (dezactivati posibilitatea de Undo pe interfata)
    get {
      return _cursor == -1;
    }
  }
  public bool OnLast { 
    // Not Redo (dezactivati posibilitaeta de Redo pe interfata)
    get {
      return _cursor == _commands.Count - 1;
    }
  }
}

// Clasa asupra carei sunt executate comenzile
public class Receiver {
  HistoryList History { get; set; }
  public Receiver() {
    History = new HistoryList();
  }
  //presupunem ca se executa la actionarea comenzii "Add" - adaugare Contact nou

  private void Add_Click(object sender, EventArgs e) {
    //vom crea obiectul Contact cu datele preluate de pe interfata
    var c = new Contact();
    var command = new AddContact(position, c, this);
    History.Add(command); //se va adauga pe ultima pozitie (cursor corect pozitionat)
    command.Execute(); //execut comanda
  }

  //la actionarea comenzii "Undo" - anulare ultima comanda
  private void Undo_Click(object sender, EventArgs e) {
    if (!History.BeforeFirst) //se recomanda sa dezactivati butonul Undo pe interfata
    {
      History.Undo(); //anuleaza comanda de la pozitia cursorului
    }
  }

  //la actionarea comenzii "Redo" - anulare ultima comanda
  private void Redo_Click(object sender, EventArgs e) {
    if (!History.OnLast) //se recomanda sa dezactivati butonul Undo pe interfata
    {
      History.Redo(); //anuleaza comanda de la pozitia cursorului
    }
  }
}
```

### Proiect 3

Să se implementeze o aplicație pentru o agendă de contacte cu posibilitatea anulării / refacerii ultimelor $n$ comenzi efectuate. Comenzile anulabile: `add`, `edit`, `delete`. O comandă va avea un nume unic.

**Facilitați**:

- adăugare contact (undo/redo)
- eliminare contact (undo/redo)
- editare (modificare contact) (undo/redo)
- undo
- redo
- afișarea conținutului listei HistoryList - după nume
- posibilitatea anulării mai multor comenzi (sunt selectate ultimele k comenzi din HistoryList, anulate de la ultima)
- lista ultimelor comenzi anulate va fi afișată separat (pentru a vizualiza comenzile Redo posibile)

**Date despre contact**:

- id, nume, adresa, email, telefon, website

**Salvare în baza de date**:

- Se va implementa și șablonul `TemplateMethod` pentru `DbReader` (`ContactDbReader`)!

## Command Pattern

**Intenţie**: încapsularea cererilor (acţiunilor) în obiecte pentru a putea fi memorate, re-executate, anulate (undo/redo), transmise ca parametrii.

**Motivaţie**: vezi aplicaţiile undo/redo.

**Structură**:

- **Invoker** (`HistoryList`): memorează/execută comenzi de tip `Command`
- **Receiver** (`AgendaGUI`): obiectul asupra căruia sunt executate comenzile;
- **Command**: clasa abstractă sau interfaţa ce reprezintă un contract între `Invoker`, `Receiver` şi comenzile concrete `ConcreteCommand` - o comandă specifică;

![CommandPattern](./img/CommandPattern.png)

- **Command**

```c#
namespace CommandPatternSkeleton {
  public abstract class Command {
    public string Name { get; set; }
    public abstract void Execute();
  }
}
```

- **ConcreteCommand1**

```c#
namespace CommandPatternSkeleton {
  class ConcreteCommand1: Command {
    public Receiver Receiver { get; private set; }
    public ConcreteCommand1(string name, Receiver receiver) {
      Receiver = receiver;
      Name = name;
    }
    public override void Execute() {
      Console.WriteLine($"Command {Name} is executing...");
      Receiver.Action();
    }
  }
}
```

- **ConcreteCommand2**

```c#
namespace CommandPatternSkeleton {
  class ConcreteCommand2: Command {
    public Receiver Receiver { get; private set; }
    public ConcreteCommand2(string name, Receiver receiver) {
      Receiver = receiver;
      Name = name;
    }
    public override void Execute() {
      Console.WriteLine($"Command {Name} is executing...");
      Receiver.Action();
    }
  }
}
```

- **Receiver**

```c#
namespace CommandPatternSkeleton {
  class Receiver {
    public void Action() {
      Console.WriteLine("Action on receiver is running...");
    }
  }
}
```

- **Invoker**

```c#
namespace CommandPatternSkeleton {
  // Invoker-ul este decuplat (nu stie nimic despre) de Receiver si comenzi concrete
  public class Invoker {
    public List<Command> Commands { get; private set; }
    public Invoker() {
      Commands = new List<Command>();
    }
    public void Add(Command c) {
      Commands.Add(c);
    }
    public void AddRange(List<Command> commands) {
      Commands.AddRange(commands);
    }
    public void Invoke() {
      foreach(var command in Commands)
        command.Execute();
    }
  }
}
```

- **Client**

```c#
namespace CommandPatternSkeleton {
  class Client {
    static void Main(string[] args) {
      Invoker i = new Invoker();
      Receiver r = new Receiver();
      i.AddRange(new List<Command>() {
        new ConcreteCommand1("c1", r),
          new ConcreteCommand2("c2", r),
          new ConcreteCommand1("c3", r),
      });
      i.Invoke();
      Console.ReadKey();
    }
  }
}
```

**Avantaje**: 

- sistem extensibil (se pot adăuga comenzi noi în sistem fără modificarea codului sursa actual);
- `invoker`-ul si `receiver`-ul sunt total decuplate (pot fi variate independent);

## Composite Pattern

**Scenariu**: Să se implementeze un instrument grafic pentru desenare de figuri geometrice şi picturi. O pictură este o grupare de alte figuri geometrice primitive sau alte picturi.

Apare o structură arborescentă, în care frunzele sunt primitive grafice, iar nodurile interne sunt picturi. Dorim să propunem o soluţie care tratează uniform atât primitivele grafice (frunzele din arbore) cât şi picturile (nodurile interne de tip `Picture`).

Prin urmare, pentru o tratare uniformă (care ne permite să extindem ulteior aplicaţia), avem nevoie de abstractizarea noţiunilor parte (frunză - primitivă grafică) şi întreg (nodurile interne - `Picture`)

![CompositePattern](./img/CompositePattern.png)

**Declarăm**:

- **Graphic**

```c#
abstract class Graphic {
  public abstract void Draw();
  public abstract void Resize(double);
  public abstract void MoveTo(int, int);
  public virtual void AddChild(Graphic) {} // Se potriveste doar la nodurile interne!	
}
```

- **Line**

```c#
class Line: Graphic {
  public void Draw() {}
  public void Resize(double) {}
  public void MoveTo(int, int) {}
}
```

- **Circle**

```c#
class Circle: Graphic {
  public void Draw() {}
  public void Resize(double) {}
  public void MoveTo(int, int) {}
}
```

- **Picture**

```c#
class Picture: Graphic {
  List<Graphic> _children;
  public void Draw() {
    foreach(var c in _children)
      c.Draw();
  }
  public void Resize(double) {}
  public void MoveTo(int, int) {}
  public virtual void AddChild(Graphic c) {
    _children.Add(c);
  }
}
```

### Proiect (extensie la aplicaţia Shape)

Să se ofere posibilitatea de grupare a figurilor geometrice şi obţinerii de picturii (desenate, redimensionate, mutate ca un singur obiect!)

**Composite Pattern**: tratarea uniformă a obiectelor aflate în relaţii ierarhice (arborescente) de tip parte-întreg. 

**Participanţi**: 

- **Component** (`Graphic`): clasa abstractă (interfaţă) ce reprezintă interfaţa comună a obiectelor aflate în relaţia parte-întreg; (opţional, poate să conţina metode pentru adăugare/eliminare/obţinere copil).

- **Composite** (`Picture`): reprezintă un obiect de tip întreg în cadrul ierarhiei; colecţie (prin agregare sau compunere) de obiecte de tip `Component`;

- **Leaf** (`Line`, `Circle`): reprezintă o "frunză" in structura arborescentă.

**Aplicabilitate**: În aplicaţii cu structuri arborescente de tip parte-întreg, care se doresc
a fi tratate uniform.

![CompositePicture](./img/CompositePicture.png)

**Observaţie**:

Dacă vom combina şablonul `Composite` cu şablonul `Command`, putem obţine
obiecte de tip "colecţii de comenzi" - un batch de comenzi ce pot fi executate ca şi cum ar fi o singură comandă. (acestea se numesc de obicei macro-comenzi)

```c#
// Suita de comenzi
class MacroCommand: Command {
  public List<Command> Commands { get; private set; }
  public void Execute() {
    foreach(var c in Command)
      c.Execute();
  }
}
```

### Aplicaţie la Composite

Evaluarea expresiilor aritmetice $4+5*(x-y)$ şi afişarea lor în: "preordine" (RSD), "inordine" (SRD), "postordine" (SDR).

**Soluţia** va fi astfel încât să tratăm uniform atât frunzele (constantele şi variabilele), cât
şi operatorii (ce pot fi binari sau unari).

**Operatori de forma**: $e_1+e_2$, $e_1-e_2$, $e_1*e_2$, $e_1/e_2$ unde $e_1$, $e_2$ sunt expresii artimetice, constante sau variabile.

În continuare, prin compunere de operatori (de aici şi şablonul `Composite`), vom obţine expresii artimetice mai complexe.

![Expressions](./img/Expressions.png)

- **Expression**

```c#
// Expresie abstracta
namespace CompositeExpression {
  abstract class Expression {
    public abstract string Inord();
    public abstract string Preord();
    public abstract string Postord();
    public abstract int Evaluate();
  }
}
```

- **Constant**

```c#
namespace CompositeExpression {
  public class Constant: Expression {
    public int Value { get; private set; }
    public Constant(int value) {
      Value = value;
    }
    public override int Evaluate() {
      return Value;
    }
    public override string Inord() {
      return Value.ToString();
    }
    public override string Postord() {
      return Value.ToString();
    }
    public override string Preord() {
      return Value.ToString();
    }
  }
}
```

- **Variable**

```c#
namespace CompositeExpression {
  public class Variable: Expression {
    public string Name { get; private set; }
    public int Value { get; set; }
    public Variable(string name, int value) {
      Name = name;
      Value = value;
    }
    public override int Evaluate() {
      return Value;
    }
    public override string Inord() {
      return $"{Name}[{Value}]";
    }
    public override string Postord() {
      return $"{Name}[{Value}]";
    }
    public override string Preord() {
      return $"{Name}[{Value}]";
    }
  }
}
```

- **Binary**

```c#
namespace CompositeExpression {
  public abstract class Binary: Expression {
    public Expression Left { get; private set; }
    public Expression Right { get; private set; }
    public string OpSymbol { get; }
    public Binary(Expression left, Expression right, string op) {
      Left = left;
      Right = right;
      OpSymbol = op;
    }
    public override string Inord() {
      return $"{Left.Inord()}{OpSymbol}{Right.Inord()}";
    }
    public override string Postord() {
      return $"{Left.Inord()}{Right.Inord()}{OpSymbol}";
    }
    public override string Preord() {
      return $"{OpSymbol}{Left.Inord()}{Right.Inord()}";
    }
  }
}
```

- **Sum, Mult, Dif**

```c#
namespace CompositeExpression {
  public class Sum: Binary {
    public Sum(Expression left, Expression right, string op): base(left, right, op) {}
    public override int Evaluate() {
      return Left.Evaluate() + Right.Evaluate();
    }
  }

  public class Mult: Binary {
    public Mult(Expression left, Expression right, string op): base(left, right, op) {}
    public override int Evaluate() {
      return Left.Evaluate() * Right.Evaluate();
    }
  }

  public class Dif: Binary {
    public Dif(Expression left, Expression right, string op): base(left, right, op) {}
    public override int Evaluate() {
      return Left.Evaluate() - Right.Evaluate();
    }
  }
}
```

- **Program**

```c#
namespace CompositeExpression {
  class Program {
    static void Main(string[] args) {
      //4 + 5 * (x-y)
      var e = new Sum(new Constant(4), new Mult(new Constant(5), new Dif(new Variable("x", 2), new Variable("y", 3), "-"), "*"), "+");
      Console.WriteLine(e.Inord());
      Console.WriteLine(e.Preord());
      Console.WriteLine(e.Postord());
      Console.WriteLine("e(2,3)={0}", e.Evaluate());
      Console.ReadKey();
    }
  }
}
```

## Abstract Factory Pattern

(Fabrici abstracte de obiecte)

**Scenariu**: Desenarea componentelor GUI astfel încât aplicaţia să suporte "multiple look and feel".

```c#
abstract class Button {
  public abstract void Paint();
}
class WinButton: Button {
  public abstract void Paint() {
    // desenare button specific platformei Windows
  }
}
class MacButton: Button {
  public abstract void Paint() {
    // desenare button specific platformei Mac
  }
}
abstract class Window {
  public abstract void Paint();
}
class WinWindow: Window {
  public abstract void Paint() {
    // desenare Windowspecific platformei Windows
  }
}
class MacWindow: Window {
  public abstract void Paint() {
    // desenare buWindowtton specific platformei Mac
  }
}
```

Cum sunt create (dinamic) obiecte din diverse familii (ierarhii)? (creare dinamică, la execuţie; asociere corectă de obiecte)

**Soluţia**: Abstract Factory

```c#
abstract class AbstractFactory {
  public abstract Button CreateButton();
  public abstract Window CreateWindow();
}

// Implementam fabrici concrete
class WinFactory: AbstractFactory {
  public Button CreateButton() {
    return new WinButton();
  }
  public Window CreateWindow() {
    return new WinWindow();
  }
}

class MacFactory: AbstractFactory {
  public Button CreateButton() {
    return new MacButton();
  }
  public Window CreateWindow() {
    return new MacWindow();
  }
}

//DIP: programeaza la nivel abstract (la nivel de interfata).
class Application {
  AbstractFactory Factory { get; set; }
  public void PaintComponents() {
    var button = Factory.CreateButton();
  }
}

var a = new Application();
a.Factory = new WinFactory(); // sau utilizam o librarie IoC (Inversion of Control) pentru dependency injection(Ninject)
```

**Avantaje**:

- Extensibilitate: 
  - pot fi considerate noi tipuri de obiecte în sistem, din familii diferite;
  - tipul obiectelor poate fi selectat dinamic, la momentul execuţiei;
  - este asigurată gruparea corectă a obiectelor!

