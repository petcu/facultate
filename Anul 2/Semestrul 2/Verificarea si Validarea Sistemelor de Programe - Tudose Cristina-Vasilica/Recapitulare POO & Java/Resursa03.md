# Resursă 03

[Ştefan Tanasă, Cristian Olaru, Ştefan Andrei] - Java de la 0 la expert

Răspunsuri grilă: https://profs.info.uaic.ro/~stanasa/java/core/index.html  

Link carte:

- Scribd: https://www.scribd.com/document/389691664/java-de-la-0-la-expert-pdf-pdf
- DLSCRIB: https://dlscrib.com/download/java-de-la-0-la-expert-pdf_58e2763ddc0d601d748970f3_pdf
- KuPdf: https://kupdf.net/download/java-de-la-0-la-expert-pdf_58e2763ddc0d601d748970f3_pdf