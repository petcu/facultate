# Laborator 06

[TOC]

## Date: 07.04.2021

## Part 1

![l06p1_1](./img/lab06/l06p1_1.png)

![l06p1_2](./img/lab06/l06p1_2.png)

![l06p1_3](./img/lab06/l06p1_3.png)

## Part 2

![l06p2_1](./img/lab06/l06p2_1.png)

![l06p2_2](./img/lab06/l06p2_2.png)

