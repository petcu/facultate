# Laborator 03

## Date: 17.03.2021

[TOC]

## Part 1

![l03p1_1](./img/lab03/l03p1_1.png)

![l03p1_2](./img/lab03/l03p1_2.png)

## Part 2

![l03p2_1](./img/lab03/l03p2_1.png)

![l03p2_2](./img/lab03/l03p2_2.png)

![l03p2_3](./img/lab03/l03p2_3.png)

