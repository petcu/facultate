# Laborator 01

## Date: 03.03.2021 

[TOC]

## Part 1

![l01p1_1](./img/lab01/l01p1_1.png)

![l01p1_2](./img/lab01/l01p1_2.png)

![l01p1_3](./img/lab01/l01p1_3.png)

## Part 2

![l01p2_1](./img/lab01/l01p2_1.png)

![l01p2_2](./img/lab01/l01p2_2.png)

![l01p2_3](./img/lab01/l01p2_3.png)



