# Curs14 - Rezolvare

[TOC] 

## Exerciţiu 2. c)

![Ex2c](./img/Ex2c.png)

## Exerciţiu 4. a)

![Ex4a-1](./img/Ex4a-1.png)

![Ex4a-2](./img/Ex4a-2.png)

## Exerciţiu 4. b)

![Ex4b-1](./img/Ex4b-1.png)

![Ex4b-2](./img/Ex4b-2.png)

![Ex4b-3](./img/Ex4b-3.png)

![Ex4b-4](./img/Ex4b-4.png)

