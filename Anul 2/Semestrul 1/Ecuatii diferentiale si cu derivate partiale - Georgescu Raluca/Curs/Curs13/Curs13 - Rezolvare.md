# Curs13 - Rezolvare

[TOC]

### Exemplu 1

![Exemplu1](./img/Exemplul1.png)

### Exemplu 2 - [Video](./video/Exemplul2.mp4)

![Exemplu2-1](./img/Exemplul2-1.png)

![Exemplu2-2](./img/Exemplul2-2.png)

### Exemplu 3

![Exemplu3-1](./img/Exemplul3-1.png)

![Exemplu3-2](./img/Exemplul3-2.png)

### Formare Sistem Simetric

![Formule-Sistem-Simetric](./img/Formare_sistem_simetric.png)

