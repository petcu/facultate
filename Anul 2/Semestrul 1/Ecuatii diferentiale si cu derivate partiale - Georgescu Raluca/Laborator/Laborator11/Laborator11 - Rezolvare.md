# Laborator11 - Rezolvare

[TOC]

### Exerciţiu 1. a) - [Video](./video/Ex1a.mp4)

![Ex1a](./img/Ex1a.png)

### Exerciţiu 1. c)

![Ex1c](./img/Ex1c.png)

### Exerciţiu 2. b) - [Video](./video/Ex2b.mp4)

![Ex2b](./img/Ex2b.png)

### Exerciţiu 3. a) - [Video](./video/Ex3a.mp4)

![Ex3a-pag1](./img/Ex3a-pag1.png)

![Ex3a-pag2](./img/Ex3a-pag2.png)

