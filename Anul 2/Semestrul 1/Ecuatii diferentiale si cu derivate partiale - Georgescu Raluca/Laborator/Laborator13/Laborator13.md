# Laborator13

[TOC]

## Enunţ

$$
\large
\begin{align*}
& x ^ \prime = x + y + e^t \cdot cost\\
& y ^ \prime = -x + y - e^t \cdot sint
\end{align*}
$$

## Rezolvare

![Ex1](./img/Ex1.png)

![Ex2](./img/Ex2.png)

![Ex3](./img/Ex3.png)