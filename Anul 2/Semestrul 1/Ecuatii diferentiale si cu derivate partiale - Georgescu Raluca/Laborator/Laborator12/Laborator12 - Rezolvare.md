# Laborator12 - Rezolvare

[TOC]

## Exerciţiu 1.a - [Video](./video/Ex1a.mp4)

![Ex1a-1](./img/Ex1a-1.png)

![Ex1a-2](./img/Ex1a-2.png)

![Ex1a-3](./img/Ex1a-3.png)

## Exerciţiu 2.a - [Video](./video/Ex2a.mp4)

![Ex2a-1](./img/Ex2a-1.png)

![Ex2a-2](./img/Ex2a-2.png)

## Exerciţiu 3.a - [Video](./video/Ex3a.mp4)

![Ex3a](./img/Ex3a.png)

## Exerciţiu 3.b

![Ex3b](./img/Ex3b.png)

