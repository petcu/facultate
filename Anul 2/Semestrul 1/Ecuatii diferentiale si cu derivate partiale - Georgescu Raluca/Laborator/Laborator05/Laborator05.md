# Laborator05

[TOC]

## Enunţuri

Să se rezolve următoarele ecuaţii:
$$
\begin{align*}
1.\ &\ t \cdot x^{\prime} = x^3 + x \\
2.\ &\ x^{\prime} = \frac{t + 2x + 1}{2t + 4x + 3} \\
3.\ &\ x^{\prime} = 4\frac{x}{t} + t\sqrt{x} \\
4.\ &\ x^{\prime} = \frac{x}{t} + tg\frac{x}{t}\\
5.\ &\ (t^2 + tx + x^2)dt - t^2dx = 0 \\
\end{align*}
$$

## Rezolvare

### Exerciţiu 01

![Ex1](./img/Ex1.png)

### Exerciţiu 02

![Ex2](./img/Ex2.png)

### Exerciţiu 03

![Ex3](./img/Ex3.png)

### Exerciţiu 04

![Ex4](./img/Ex4.png)

### Exerciţiu 05

![Ex5](./img/Ex5.png)

