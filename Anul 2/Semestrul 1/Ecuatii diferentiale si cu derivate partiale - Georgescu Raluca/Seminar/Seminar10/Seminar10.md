# Seminar10

[TOC]

## Ecuaţii 

Să se rezolve următaorele ecuaţii:
$$
\large
\begin{align*}
&a)\ x^{II}- 2x^{I} + 5x = e^t (t \cos 2t - t^2 \sin 2t) \\
&b)\ x^{II} + 9x = t \sin t + \cos 3t\\
&c)\ x^{II} - 9x = e^{2t} + te^{t} - t^2 - 2\\
\end{align*}
$$


