# Seminar05

##  Aplicatii ale ecuatiilor diferentiale (continuare)

[TOC]

### 1.

Ecuaţia diferenţială pentru familia curbelor definite de ecuaţia exponenţială $\large y = e^{x+c}$ este $\large y^{\prime} - y = 0$.

**Rezolvare**: Prin diferenţierea ecuaţiei în raport cu $\large x$ obţinem $\large y^{\prime} = e^{x+c}$. 

Putem elimina cu uşurinţă parametrul $\large c$ din sistemul de ecuaţii:
$$
\large
\begin{cases}
y^{\prime} = e^{x+c} \\
y = e^{x+c}
\end{cases}
$$
de unde rezultă $\large y^{\prime} = y$, $\large y^{\prime} - y = 0$, care este o ecuaţie diferenţială cu variabile separabile.

### 2.

Ecuaţia diferenţială pentru familia de parabole definite de ecuaţia $\large y = x^2 - cx$ este $\large y^{\prime}x + y = 3x^2$.

**Rezolvare**: Diferenţiem ecuaţia implicită şi obţinem $\large y^{\prime} = 2x - c$.

Scriem această ecuaţie împreună cu ecuaţia algebrică originală şi eliminăm parametrul $\large c$.
$$
\large
\begin{cases}
y^{\prime} = 2x-c\\
y = x^2 - cx
\end{cases}
$$
Observăm că $\large c = y^{\prime} - 2x$ din prima ecuaţie şi înlocuim în a doua ecuaţie $\large y = x^2 - (y^{\prime} - 2x)x$ $\large \iff$ $\large y = x^2 - y^{\prime}x + 2x^2$.

Am obţinut o ecuaţie diferenţială implicită corespunzătoare familiei de curbe place $\large y^{\prime}x + y = 3x^2$ care este o ecuaţie afină.

## Rezolvare

### Exerciţiu 01 & 02

![Ex1,2.png](./img/Ex1,2.png)

