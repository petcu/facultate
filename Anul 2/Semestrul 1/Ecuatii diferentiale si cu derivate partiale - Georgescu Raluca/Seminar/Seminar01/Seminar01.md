# Seminar01

[TOC]

## Enunţuri

Să se determine următoarele primitive, care îndeplinesc condiţiile specificate:

$$
\begin{align*}
1)& \int \frac{ln(x)}{x}dx;\ F(1) = 2 \\
2)& \int ln^2x\ dx;\ F(1) = 1 \\
3)& \int xe^{x+2}\ dx;\ F(0) = 2 \\
4)& \int (x+x^3)e^{x^2}\ dx; F(0) = 3 \\
5)& \int [x + ln(1 + x^2)]\ dx;\ F(0) = 1\\
\end{align*}
$$

## Rezolvare

### Exerciţiu 1. şi Exerciţiu 2.

![Ex1,2](.Seminar01/Ex1,2.png)

### Exerciţiu 3. şi Exerciţiu 4.

![Ex3,4](.Seminar01/Ex3,4.png)

### Exerciţiu 5.

![Ex5](.Seminar01/Ex5.png) 

