# Enunt - 2020.10.31

Pentru activitatea de laborator, aici trebuie sa incarcati aplicatiile facute de voi in cadrul laboratoarelor. (tot proiectul)

1. Incercati, va rog, sa creati o multime de numere complexe (in main-ul  de la exemplul pentru clase generice) si sa va "jucati" cu ea, apeland  mai multe metode specifice clasei.

2. Va rog sa incercati sa terminati aplicatia din **Cursul 2**.

***Indicii\***: Evenimentele de tip **validating** pe componentele de tip **TextBox** asociate notelor la **examen****,** **[laborator](https://learn.upit.ro/mod/folder/view.php?id=23962)** se genereaza automat din fereastra **Properties**, de la **Events**. Metoda **NotaValida** nu este generata automat.

3. Conectarea din C# la o baza de date si realizarea principalelor operatii (select, insert, update, delete) (Cursul 3) 

