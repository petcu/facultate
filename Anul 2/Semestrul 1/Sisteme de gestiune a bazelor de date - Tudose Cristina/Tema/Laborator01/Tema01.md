# Tema - Laborator01

1. De completat clasa **Multime**:

- supraincarcand operatorii * (pentru intersectia a doua multimi), - (pentru scaderea a doua multimi)

- apeland toate metodele create in `Main()`

2. Creati, in mod asemanator, clasa `NumarComplex` care sa contina:

- doua atribute **re, im** de tip intreg pentru partea reala si partea imaginara. (`z = re + im *i`)

- trei constructori (implicit, de initializare, de copiere)

- accesori

- metoda `ToString()`

- supraincarcarea operatorilor + (pentru suma a doua numere complexe), * (pentru produsul a doua numere complexe)

- o metoda ce calculeaza modului unui numar complex

Toate metodele definite trebuie apelate in programul principal. 